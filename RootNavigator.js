
import * as React from 'react';
import { View, Text } from 'react-native';
import { createAppContainer ,createSwitchNavigator} from 'react-navigation';
import { createStackNavigator ,Header} from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';



//screens
import Splash from './src/screens/Splash.js';
import LoginContainer from './src/screens/LoginContainer.js';
import ForgotPassword from './src/screens/ForgotPassword.js';
import ResetPassword from './src/screens/ResetPassword.js';
import Otp from './src/screens/Otp.js';
import SignIn from './src/screens/SignIn.js';
import Home from './src/screens/Home.js';
import Drawer from './src/screens/Drawer.js';
import Products from './src/screens/Products.js';
import ProductDetails from './src/screens/ProductDetails.js';
import ContactUs from './src/screens/ContactUs.js';
import Notifications from './src/screens/Notifications.js';
import MyOrders from './src/screens/MyOrders.js';
import Cart from './src/screens/Cart.js';
import MyAccount from './src/screens/MyAccount.js';
import EditProfile from './src/screens/EditProfile.js';
import Privacy from './src/screens/Privacy.js';
import ChangePassword from './src/screens/ChangePassword.js';
import AddAddress from './src/screens/AddAddress.js';
import MyAddress from './src/screens/MyAddress.js';
import CheckOut from './src/screens/CheckOut.js';
import OrderPlaced from './src/screens/OrderPlaced.js';
import ShowProducts from './src/screens/ShowProducts.js';
import ViewPdf from './src/screens/ViewPdf.js';
console.disableYellowBox = true;

//******************************** Cart  Stack ********************************************** */

const cartStack = createStackNavigator({
  Cart :{ screen: Cart},
  OrderPlaced :{ screen: OrderPlaced},
  CheckOut :{ screen: CheckOut},
  AddAddress :{ screen: AddAddress},
}, {
     headerMode: 'none',
     initialRouteName: 'Cart',
     navigationOptions: {
      gesturesEnabled: true
    }
})


//******************************** Notifications  Stack ********************************************** */

const notificationStack = createStackNavigator({
  Notifications :{ screen: Notifications},
  Cart :{screen:cartStack},
}, {
     headerMode: 'none',
     initialRouteName: 'Notifications',
     navigationOptions: {
      gesturesEnabled: true
    }
})



//******************************** Contact Us  Stack ********************************************** */

const contactStack = createStackNavigator({
  ContactUs :{ screen: ContactUs},
  Cart :{screen:cartStack},
  Notifications :{screen:notificationStack},
}, {
     headerMode: 'none',
     initialRouteName: 'ContactUs',
     navigationOptions: {
      gesturesEnabled: true
    }
})



//******************************** MyOrders  Stack ********************************************** */

const myOrdersStack = createStackNavigator({
  MyOrders :{ screen: MyOrders},
  Cart :{screen:cartStack},
  Notifications :{screen:notificationStack},
}, {
     headerMode: 'none',
     initialRouteName: 'MyOrders',
     navigationOptions: {
      gesturesEnabled: true
    }
})




//******************************** Privacy Policy  Stack ********************************************** */

const privacyStack = createStackNavigator({
  Privacy :{ screen: Privacy},
  Cart :{screen:cartStack},
  Notifications :{screen:notificationStack},
}, {
     headerMode: 'none',
     initialRouteName: 'Privacy',
     navigationOptions: {
      gesturesEnabled: true
    }
})



//******************************** My Account  Stack ********************************************** */

const myAccountStack = createStackNavigator({
  MyAccount :{ screen: MyAccount},
  EditProfile :{ screen: EditProfile},
  ChangePassword :{ screen: ChangePassword},
  AddAddress :{ screen: AddAddress},
  MyAddress :{ screen: MyAddress},
  Cart :{screen:cartStack},
  Notifications :{screen:notificationStack},
  MyOrders :{screen:MyOrders},
}, {
     headerMode: 'none',
     initialRouteName: 'MyAccount',
     navigationOptions: {
      gesturesEnabled: true
    }
})





//******************************** Home Stack ********************************************** */

const homeStack = createStackNavigator({
 
  Home :{ screen: Home},
  Products :{ screen: Products},
  ProductDetails :{ screen: ProductDetails},
  ViewPdf :{ screen: ViewPdf},
  Cart :{screen:cartStack},
  ShowProducts :{screen:ShowProducts},
  Notifications :{screen:notificationStack},
 
}, {
     headerMode: 'none',
     initialRouteName: 'Home',
     navigationOptions: {
      gesturesEnabled: true
    }
})


//******************************** Home Drawer ********************************************** */


const homeDrawer = createDrawerNavigator({
  Home :{ screen: homeStack},
  ContactUs :{screen:contactStack},
  Notifications :{screen:notificationStack},
  MyOrders :{screen:myOrdersStack},
  MyAccount :{screen:myAccountStack},
  Privacy :{screen:privacyStack},
 
}
, {
  initialRouteName: 'Home',
  gesturesEnabled: true,
  mode: 'modal',
  contentComponent: props => <Drawer {...props}/>
})


//******************************** Login Stack ********************************************** */

const loginStack = createStackNavigator({
 
  Login :{ screen: LoginContainer},
  ForgotPassword :{ screen: ForgotPassword},
  SignIn :{ screen: SignIn},
  Otp :{ screen: Otp},
  ResetPassword :{ screen: ResetPassword},
  

}, {
     headerMode: 'none',
     initialRouteName: 'Login',
     navigationOptions: {
      gesturesEnabled: true
    }
})




//******************************** App Stack ********************************************** */


const AppStack = createSwitchNavigator({
  Splash : { screen: Splash},
  Login : { screen: loginStack},
  Home:{screen: homeDrawer},

}, {
     headerMode: 'none',
     initialRouteName: 'Splash',
     navigationOptions: {
      gesturesEnabled: false
    }

})


const RootNavigator = createAppContainer(AppStack)
export default RootNavigator;
