// import { 
// 	API_REQUEST, 
// 	API_RESPONSE,
// } from '../Actions/UserActions'

const initialState = {
	isFetching: false,
};

 function apiReducer(state = initialState, action) {
	switch(action.type) {
		case 'API_REQUEST':
			return { 
				...state, 
				isFetching: true,
			
			}
		case 'API_RESPONSE':
			return { 
				...state, 
				isFetching: false,
				
			}
			
		default:
			return state
	}
}

export default apiReducer