import {Dimensions} from 'react-native';

export const colors = {
  STATUS_BAR_COLOR: "#0C1532",
  COLOR_PRIMARY: "#032A6B",
  COLOR_SECONDARY:'#6C93D1',
  WHITE:'#FFFFFF',
  BLACK:'#000000',
  LIGHT_MAROON:'#301606',
  DARK_MAROON:'#170B03',
  LIGHT_YELLOW:'#FAFAD2',
  SILVER:' #C0C0C0',
  LIGHT_DARK:'#1e1e1e',
  LIGHT_GREEN:'#7CFC00',
  CRIMSON:'#DC143C',
  GREEN:'#008000',
  MAROON:'#2f0909',
  PALE_GREEN:'#98FB98',
  GREY:'#D3D3D3',
  DARK_GREY:'#C0C0C0',
  RED: "#ec0008",
  PAGE_BACKGROUND:'#F6F4FC'

};



export const urls = {
 BASE_URL: 'https://webmobril.org/dev/iqrpro_app/api/v1/',
 BASE_URL_MEDIA: 'https://webmobril.org/dev/iqrpro_app/',
};


export const dimensions = {
  SCREEN_WIDTH : Dimensions.get('window').width,
  SCREEN_HEIGHT : Dimensions.get('window').height
};



export const values = {
 // STRIPE_DEV_KEY :'pk_test_lH3nLCADTo6rIyxvns0P8igB008LHXo6qB',
 STRIPE_DEV_KEY : 'pk_test_677N60UXcI3lJUPvNhH65gH100QZSdfGcU'
 
};

