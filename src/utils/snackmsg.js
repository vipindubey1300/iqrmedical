import Snackbar from 'react-native-snackbar';
import {colors,urls,dimensions} from '../utils/constants';


export const showMessage = async (message,error = true) => {
  Snackbar.show({
    title: message,
    duration: Snackbar.LENGTH_SHORT,
    backgroundColor: error ?  'black' : colors.COLOR_PRIMARY,
    color: error ?  'red' : 'white',
   
  });
}

