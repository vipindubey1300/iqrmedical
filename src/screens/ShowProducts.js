
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,FlatList} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  
  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import CustomTextInput from '../components/CustomTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import Header from '../components/Header';
  import SearchHeader from '../components/SearchHeader';
  import ProductsComponent from '../components/ProductsComponent';
  import ProgressBar from '../components/ProgressBar';

  //redux
  import { connect } from 'react-redux';
  import { addUser ,apiRequest,apiResponse} from '../actions/actions';


class ShowProducts extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        products:[]
      }
    }




  
    componentDidMount(){
        var result  = this.props.navigation.getParam('result')
        var products = result.products
  
        this.setState({products:products})
      }
    
    onProductsClick = (item) =>{
      console.log('onProductsClick--------',JSON.stringify(item))
      let obj={'product':item}
      this.props.navigation.navigate('ProductDetails',{result:obj})
    }


  
   _renderItemProducts = ({item, index}) =>{
   
   return (
      <ProductsComponent
       object = {item} 
       index ={index}
       {...this.props}
       clickHandler ={this.onProductsClick}/>
   )
 }
    
  
      render() {
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                     <Header  label={'Products'} {...this.props}/>
                  
                      <KeyboardAwareScrollView
                      showsVerticalScrollIndicator={false}
                      contentContainerStyle={styles.scrollContainer}>

                        <View style={{padding:10,width:dimensions.SCREEN_WIDTH}}>

                            <FlatList
                            style={{marginVertical:10}}
                            data={this.state.products}
                            showsVerticalScrollIndicator={false}
                            renderItem={(item,index) => this._renderItemProducts(item,index)}
                            keyExtractor={item => item.id}
                        />


                        </View>
                    </KeyboardAwareScrollView>
                    { this.props.apiReducer.isFetching && <ProgressBar/> }     
          </SafeAreaView>
        </>
      );
      }
  }

  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(ShowProducts);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      alignItems:'center',
      flexGrow:1,
      backgroundColor:colors.PAGE_BACKGROUND   
    },
    searchHeader:{
        overflow:'hidden',
        elevation:0
    },
    categoriesContainer:{
      padding:10,
      backgroundColor:'white',
      elevation:2,
      marginVertical:10,
      shadowColor: '#000000',
      shadowOffset: { width: 1, height: 1 },
      shadowOpacity:  0.2,
    },
    label:{marginTop:15,color:'grey',fontSize:18,fontWeight:'bold'}
   
  })
  
  