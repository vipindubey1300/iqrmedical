
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,FlatList} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  import { StackActions, NavigationActions} from 'react-navigation';

  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import UnderlineTextInput from '../components/UnderlineTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import Header from '../components/Header';
  import ProgressBar from '../components/ProgressBar';

  
  //redux
  import { connect } from 'react-redux';
  import { addUser ,apiRequest,apiResponse} from '../actions/actions';

 class OrderPlaced extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        total:0
        
      }
    }

   
    _onPlaced =()=>{
      this.props.navigation.navigate('Home');

      const resetAction = StackActions.reset({
      index: 0,
      key: 'Home',
      actions: [NavigationActions.navigate({ routeName: 'Home' })],
    });

    this.props.navigation.dispatch(resetAction);
    }


    componentWillMount(){
      var result  = this.props.navigation.getParam('result')
      var amount = result.amount
      
      this.setState({
        total:amount,
       
      })
    }
  
  
      render() {
          
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                     <Header  label={'Order Placed'} {...this.props}/>
                  
                      <KeyboardAwareScrollView
                      showsVerticalScrollIndicator={false}
                      contentContainerStyle={styles.scrollContainer}>

                        <View style={{padding:10,width:dimensions.SCREEN_WIDTH,alignItems:'center'}}>


                        <FastImage 
                        source={require('../assets/order-placed.png')}
                        style={{width:150,height:150,marginTop:50}} 
                        resizeMode={FastImage.resizeMode.cover}/>



                        <View style={styles.topContainer}>
                            <Text style={{color:'black',fontWeight:'bold',fontSize:21,marginVertical:5}}>${parseFloat(this.state.total).toFixed(2)}</Text>
                            <Text style={{color:'grey',marginVertical:5}}>Your Payment is complete</Text>
                            <Text style={{color:'grey',marginVertical:5}}>Please check the delivery status at</Text>
                            <Text style={{color:'grey',marginVertical:5}}><Text style={{color:colors.COLOR_PRIMARY,marginVertical:5,fontWeight:'bold'}}>Order Tracking</Text>  Page</Text>


                        </View>


                        <ButtonComponent 
                        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:40}}
                        handler={this._onPlaced}
                        label ={'Continue Shopping'}/>

                        
                       
                       
                       

                        </View>

                    </KeyboardAwareScrollView>
                    { this.props.apiReducer.isFetching && <ProgressBar/> }     
          </SafeAreaView>
        </>
      );
      }
  }

  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(OrderPlaced);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      alignItems:'center',
      flexGrow:1,
      backgroundColor:colors.PAGE_BACKGROUND   
    },
    topContainer:{
        width:dimensions.SCREEN_WIDTH * 0.95,
        padding:12,
        backgroundColor:'white',
        marginVertical:15,
        alignItems:'center'
     },
    
   
   
  })
  
  