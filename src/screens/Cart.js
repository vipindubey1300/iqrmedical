
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,FlatList} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  
  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import CustomTextInput from '../components/CustomTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import Header from '../components/Header';
  import SearchHeader from '../components/SearchHeader';
  import CartComponent from '../components/CartComponent';
  import ProgressBar from '../components/ProgressBar';

  //redux
  import { connect } from 'react-redux';
  import { addUser ,apiRequest,apiResponse,removeUser} from '../actions/actions';
 const products = [
  {
      "id" :'1',
      "name": "Surgical Gowns",
      'description':"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "image": require('../assets/img4.png'),
      'price':344
  },
  {
    "id" :'1',
    "name": "Surgical Gowns",
    'description':"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    "image": require('../assets/img5.png'),
    'price':344
},


]
  
 class Cart extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        address:[],
        selectedAddress:null,
        carts:[],
        subtotal:0,
        shipping:0,
        tax:0,
        total:0
      }
    }

    _fetchAddress = () =>{
      var formData = new FormData();
      formData.append('userid',this.props.rootReducer.user.id);
    
      this.props.fetch()
      let url = urls.BASE_URL +'api_get_address'
            
         fetch(url, {
             method: 'POST',
             body: formData
            }).then((response) => response.json())
                 .then( (responseJson) => {
                  this.props.fetchCancel()
                  console.log(responseJson)
                  if (!responseJson.error){

                      this.setState({address:responseJson.result})
                            
                     }else{
  
                         showMessage(responseJson.message)
                      
                    }
                 }).catch((error) => {
                   console.log(error.message)
                           this.props.fetchCancel()
                           showMessage('Try Again')
                 });
    
  
   }


   _fetchCartItems = () =>{
    var formData = new FormData();
    formData.append('userid',this.props.rootReducer.user.id);
  
    this.props.fetch()
    let url = urls.BASE_URL +'api_cart_items'
          
       fetch(url, {
           method: 'POST',
           body: formData
          }).then((response) => response.json())
               .then( (responseJson) => {
                this.props.fetchCancel()
                console.log('--->>>>>>>>>>>>',responseJson)
                if (!responseJson.error){

                    this.setState({
                      carts:responseJson.result,
                      subtotal:responseJson.carttotal,
                      tax:responseJson.tax,
                      shipping:responseJson.shipping_cost,
                      total:  parseInt(responseJson.carttotal) + parseInt(responseJson.tax) + parseInt(responseJson.shipping_cost)
                    })
                          
                   }else{
                    this.setState({
                      carts:[],
                      subtotal:0,
                      tax:0,
                      shipping:0,
                      total: 0
                    })

                      // showMessage(responseJson.message)
                    
                  }
               }).catch((error) => {
                 console.log(error.message)
                         this.props.fetchCancel()
                         showMessage('Try Again')
               });
  

 }



 _deleteCartItem = (cart_id) =>{
  var formData = new FormData();
  formData.append('userid',this.props.rootReducer.user.id);
  formData.append('cart_id',cart_id);

  this.props.fetch()
  let url = urls.BASE_URL +'api_remove_cart'
        
     fetch(url, {
         method: 'POST',
         body: formData
        }).then((response) => response.json())
             .then( (responseJson) => {
              this.props.fetchCancel()
              console.log(responseJson)
              if (!responseJson.error){

                   this._fetchCartItems()
                        
                 }else{

                   showMessage(responseJson.message)
                  
                }
             }).catch((error) => {
                       console.log(error.message)
                       this.props.fetchCancel()
                       showMessage('Try Again')
             });


}


    _onCheckout =()=>{
      
      if(this.state.carts.length == 0) showMessage('No items in cart')
      else if(this.state.address.length == 0) showMessage('Add an address to continue')
      else if(this.state.selectedAddress == null) showMessage('Choose Address first')
      else {
        let obj ={
          'amount':this.state.total,
          'shipping':this.state.shipping,
          'tax':this.state.tax,
          'address_id':this.state.selectedAddress.id
        }
        this.props.navigation.navigate("CheckOut",{result:obj})
      }
     
  }
    
    onProductsClick = (item) =>{
      console.log('onProductsClick--------',JSON.stringify(item))
      let obj={'product':item}
      
    }

    componentWillMount(){
     
      this._fetchCartItems()
       this._fetchAddress()
    }
 

    handler = () =>{
  
     
     
     }

     _onCartItemDelete = (cartId) =>{
      console.log('_onCartItemDelete--------',JSON.stringify(cartId))
      this._deleteCartItem(cartId)
    }
  
   _renderItemProducts = ({item, index}) =>{
   
   return (
      <CartComponent
       object = {item} 
       index ={index}
       {...this.props}
       clickHandler ={this.onProductsClick}
       deleteHandler ={this._onCartItemDelete}/>
   )
 }

 _renderAddress = ({item, index}) =>{
   
  const isSelectedAddress = this.state.selectedAddress == null ? false : (this.state.selectedAddress.id === item.id)
  const address = item.address_title + "," + item.address_line + "," +item.address_line2 + "," +
  item.city + "," + item.state + "," + item.country + "," +item.zip_code

     
   return(
     <TouchableOpacity onPress={()=>{
       this.setState({
         selectedAddress: item
       });
     }}
     style={{
       borderWidth:isSelectedAddress ? 3 : 1,
       borderColor:isSelectedAddress ? colors.COLOR_PRIMARY : 'grey',
       margin:10,
       width:dimensions.SCREEN_WIDTH * 0.4,
       alignItems:'center',
       justifyContent:'center',
       borderRadius:10,
      backgroundColor:'white'
      
     }}>

   

     <View style={{margin:5,justifyContent:'center',alignItems:'center'}}>



       <Text style={{textAlign:'center',fontWeight:'bold',margin:2}}>{item.name}</Text>
       <Text style={{textAlign:'center',margin:2,fontSize:13}}>{address}</Text>

       <Text style={{textAlign:'center',margin:2,fontSize:13,color:'grey'}}>{item.mobile}</Text>
     </View>
     
     
     </TouchableOpacity>
   )
  
}

_addAddress=()=>{
  let obj ={
    'path_name':'Cart',
  }
  this.props.navigation.navigate('AddAddress',{result:obj})
}
componentDidMount(){
  this.props.navigation.addListener('willFocus',this._fetchAddress);
 }

  
      render() {
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                     <Header  label={'Cart'} {...this.props}/>
                  
                      <KeyboardAwareScrollView
                      showsVerticalScrollIndicator={false}
                      contentContainerStyle={styles.scrollContainer}>
                    

                        <View style={{padding:10,width:dimensions.SCREEN_WIDTH}}>
                        {
                          this.state.carts.length > 0
                          ?
                            
                          <FlatList
                          style={{marginVertical:10}}
                          data={this.state.carts}
                          showsVerticalScrollIndicator={false}
                          renderItem={(item,index) => this._renderItemProducts(item,index)}
                          keyExtractor={item => item.id}
                        />
                        
                              : 
                              this.props.apiReducer.isFetching ?
                              null
                              :
                        
                                <View style={{justifyContent:'center',alignItems:'center'}}>
                                  <FastImage 
                                  source={require('../assets/no-cart.png')}
                                  style={{width:150,height:150,alignSelf:'center'}} 
                                  resizeMode={FastImage.resizeMode.contain}/>
                              </View>
            
                        }
            

                           

                          {
                            this.state.address.length > 0
                            ? 
                            <Text style={{color:colors.COLOR_PRIMARY,
                            marginVertical:10,fontWeight:'bold',textDecorationLine:'underline',
                          fontSize:18}}>Select Delivery Address</Text>
                            :
                           null

                          }


                          <FlatList
                            style={{marginBottom:10,width:'100%',padding:4}}
                            data={this.state.address}
                            horizontal={true}
                            extraData={this.state}
                            //contentContainerStyle={{width:'100%',padding:4}}
                            showsHorizontalScrollIndicator={false}
                            renderItem={(item,index) => this._renderAddress(item,index)}
                            keyExtractor={item => item._id}  
                         />

                          {
                            this.state.address.length > 0
                            ? null
                            :
                            <TouchableOpacity onPress={()=> this._addAddress()}
                            style={styles.addContainer}>
                              <Text>Add Address</Text>
                            </TouchableOpacity>

                          }

                        <View style={styles.priceContainer}>
                          <View style={styles.priceContainerRow}>
                            <Text style={styles.labelText}>Subtotal</Text>
                            <Text style={styles.priceText}>$ {this.state.subtotal}</Text>
                          </View>


                          <View style={styles.priceContainerRow}>
                            <Text style={styles.labelText}>Shipping </Text>
                            <Text style={styles.priceText}>$ {this.state.shipping}</Text>
                          </View>

                          <View style={styles.priceContainerRow}>
                            <Text style={styles.labelText}>Estimated Tax</Text>
                            <Text style={styles.priceText}>$ {this.state.tax}</Text>
                         </View>
                        </View>


                        <View style={styles.priceContainer}>
                          <View style={styles.priceContainerRow}>
                            <Text style={styles.labelText}>Total</Text>
                            <Text style={styles.priceText}>$ {parseFloat(this.state.total).toFixed(2)}</Text>
                          </View>
                        </View>



                        <ButtonComponent 
                        style={{width :dimensions.SCREEN_WIDTH * 0.92,marginTop:40}}
                        handler={this._onCheckout}
                        label ={'CheckOut'}/>



                        </View>
                    </KeyboardAwareScrollView>
                    { this.props.apiReducer.isFetching && <ProgressBar/> }     
          </SafeAreaView>
        </>
      );
      }
  }

  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(Cart);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      alignItems:'center',
      flexGrow:1,
      backgroundColor:colors.PAGE_BACKGROUND   
    },
    priceContainer:{
      width:dimensions.SCREEN_WIDTH * 0.95,
      padding:12,
      backgroundColor:'white',
      marginVertical:10
    },
    priceContainerRow:{
      width:'100%',
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
      marginVertical:6
    },
    labelText:{
      fontWeight:'bold',
      fontSize:15
    },
    priceText:{
      fontSize:13,
      color:'black'
    },
    addContainer:{
      paddingHorizontal:15,
      paddingVertical:7,
      justifyContent:'center',
      alignItems:'center',
      borderColor:'grey',
      borderWidth:1,
      borderRadius:10,
      margin:16
    }
   
  })
  
  