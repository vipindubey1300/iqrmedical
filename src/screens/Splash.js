
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView,Alert,Animated,Easing} from 'react-native';
import React, { useState, useEffect } from "react";
import {showMessage} from '../utils/snackmsg';
import {colors,urls,dimensions} from '../utils/constants';
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-community/async-storage';


import { connect } from 'react-redux';
import { addUser } from '../actions/actions';

class Splash extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
         
        }
    }









    home = async () =>{

      await AsyncStorage.multiGet(["id", "name","image","email",'phone']).then(response => {
        console.log(response[0][0]) // Key1
        console.log(response[0][1]) // Value1
        console.log(response[1][0]) // Key2
        console.log(response[1][1])// Value2
  
        let id = response[0][1]
        let name = response[1][1]
        let image = response[2][1]
        let email = response[3][1]
        let phone = response[4][1]
       
  
        this.props.add({ 
          id: id, 
          name : name,
          image : image,
          email:  email ,
          phone:phone,
        
        })
    })
  
     
     this.props.navigation.navigate('Home') }
  
  




  checkLoginStatus = async () =>{
    await AsyncStorage.getItem('id')
              .then((value) => {
                if(value){
                    this.home()
                }
  
                else{
                this.props.navigation.navigate('Login')
  
                }
              });
  
  }

    
  check = async () =>{
    setTimeout(() => {
            
       this.checkLoginStatus()
    }, 2000)
 
 
   }


   componentDidMount() {
    
    this.check()
    
  }





    render() {
      

      return (
        <>
          <StatusBar barStyle="light-content"  //dark-content//light-content
            backgroundColor={colors.STATUS_BAR_COLOR}/>
             <View style={styles.container}>
                <FastImage 
                  source={require('../assets/splash-bg.jpg')}
                  style={styles.splash_image} 
                  resizeMode={FastImage.resizeMode.cover}/>
             </View>


                 <FastImage 
                  source={require('../assets/logo.png')}
                  style={styles.splash_logo} 
                  resizeMode={FastImage.resizeMode.contain}/>
          
        </>
      );
    }
}

const mapDispatchToProps = dispatch => {
  return {
    // dispatching plain actions
    add: (userinfo) => dispatch(addUser(userinfo)),
  
    
  }
}

export default connect(null, mapDispatchToProps)(Splash);


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    splash_image:{
      height: dimensions.SCREEN_HEIGHT * 1, 
      width:dimensions.SCREEN_WIDTH * 1, 
    },
    splash_logo:{
      height: 200, 
      width:dimensions.SCREEN_WIDTH * 0.9, 
      position:'absolute',
      top:dimensions.SCREEN_HEIGHT * 0.15,
      right:dimensions.SCREEN_WIDTH * 0.06,
     
    },
   

})



