
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView, Keyboard} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  import AsyncStorage from '@react-native-community/async-storage';
  
  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import CustomTextInput from '../components/CustomTextInput';
  import ButtonComponent from '../components/ButtonComponent';

  
 //redux
import { connect } from 'react-redux';
import { addUser } from '../actions/actions';
      
  
  
  
  class SignUp extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
       
      }
    }



    validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }

    _isValid(){
        const name = this.nameInput.getInputValue();
        const email = this.emailInput.getInputValue();
        const password = this.passwordInput.getInputValue();
        const phone = this.phoneInput.getInputValue()
        const confirm_password = this.confirmPasswordInput.getInputValue();

     
       

        var regexp = /^\d*\.?\d*$/;
        var phoneIsNum  = regexp.test(phone);
    
    
      if(name.trim().length == 0){
        showMessage('Enter  Name')
         return false
      }
     
      else if(email.trim().length == 0){
        showMessage('Enter Email')
        return false
    
      }
      else if(!this.validateEmail(email)){
        showMessage('Enter Valid Email')
        return false
    
      }
      else if(phone.trim().length == 0){
        showMessage('Enter Phone No')
        return false
    
      }
      else if(phone.trim().length < 10){
        showMessage('Enter Valid Phone No')
        return false
    
      }else if(!phoneIsNum){
        showMessage('Enter Valid Phone No')
        return false
    
      }
      else if(password.trim().length == 0){
        showMessage('Enter Password')
        return false
    
      }
      else if (password.trim().length < 8 || password.trim().length > 16) {
        showMessage('Password should be 8-16 characters long')
        return false;
      }
      else if(confirm_password.trim().length == 0){
        showMessage('Enter Password')
        return false
    
      }
      else if (confirm_password.trim().length < 8 || confirm_password.trim().length > 16) {
        showMessage('Confirm Password should be 8-16 characters long')
        return false;
      }

      else if (confirm_password.trim() !== password.trim()) {
        showMessage('Password and Confirm Password should match')
        return false;
      }
      else{
        return true;
      }
    
    
    }
    
    
      _onSignUp = () =>{
        if(this._isValid()){

          Keyboard.dismiss()

          
        const name = this.nameInput.getInputValue();
        const email = this.emailInput.getInputValue();
        const password = this.passwordInput.getInputValue();
        const phone = this.phoneInput.getInputValue()
        const confirm_password = this.confirmPasswordInput.getInputValue();

    
      
        this.props.startApiCall()
        var formData = new FormData();
        
         formData.append('name',name);
         formData.append('email',email);
         formData.append('password', password);
         formData.append('mobile', phone);
         formData.append('confirm_password', confirm_password);
         formData.append('device_token', 'jaBBD87dg7D');
         Platform.OS =='android'
         ?  formData.append('device_type',1)
         : formData.append('device_type',2)
      
      
           console.log("FFF",JSON.stringify(formData))
      
      
                 let url = urls.BASE_URL +'api_signup'
                console.log("FFF",JSON.stringify(url))
                 fetch(url, {
                 method: 'POST',
                 headers: {
                   'Accept': 'application/json',
                   'Content-Type': 'multipart/form-data',
                 },
                 body: formData
                }).then((response) => response.json())
                     .then(async (responseJson) => {
                      this.props.endApiCall()
      
                         console.log("FFF",JSON.stringify(responseJson))

      
                      if (!responseJson.error){
      
                        var id = responseJson.result.id
                        var name = responseJson.result.name
                        var email = responseJson.result.email
                        var phone = responseJson.result.mobile
                        var image = responseJson.result.user_image

                        showMessage(responseJson.message,false)
    
                        AsyncStorage.multiSet([
                          ["id", id.toString()],
                          ["email", email],
                          ["name", name],
                          ["phone", phone],
                          ["image", image],
                         
                          ]);
  
                          console.log("Saving----",id)
    
                         await this.props.add({ 
                            id: id, 
                            phone : phone,
                            image : image,
                            email:  email ,
                            name:name,
                            
                          })
                          this.props.navigation.navigate("Home")
                                
                         }else{
    
                             showMessage(responseJson.message)
                          
                           }
                     }).catch((error) => {
                      this.props.endApiCall()
                               showMessage('Try Again')
      
                     });
        }
      
       }
    
    
  
      render() {
      return (
            <View style={styles.rootContainer}>

                <CustomTextInput
                placeholder={'Name'}
                onSubmitEditing={()=> this.email.focus()}
                inputRef={ref => this.name = ref}
                ref={ref => this.nameInput = ref}
                style={{width:'90%'}}
                autoCapitalize = 'words'
                returnKeyType="next"
                 />

                <CustomTextInput
                placeholder={'Email ID'}
                onSubmitEditing={()=> this.phone.focus()}
                inputRef={ref => this.email = ref}
                ref={ref => this.emailInput = ref}
                style={{width:'90%'}}
                returnKeyType="next"
                 />

                 <CustomTextInput
                 placeholder={'Phone Number'}
                 keyboardType="numeric"
                 textContentType='telephoneNumber'
                 onSubmitEditing={()=> this.password.focus()}
                 inputRef={ref => this.phone = ref}
                 ref={ref => this.phoneInput = ref}
                 style={{width:'90%'}}
                 returnKeyType="next"
                  />


                 <CustomTextInput
                 placeholder={'Password'}
                 secureTextEntry={true}
                 onSubmitEditing={()=> this.confirmPassword.focus()}
                 inputRef={ref => this.password = ref}
                 ref={ref => this.passwordInput = ref}
                 style={{width:'90%'}}
                 passwordType={true}
                 returnKeyType="next"
                  />


                  <CustomTextInput
                  placeholder={'Confirm Password'}
                  secureTextEntry={true}
                  onSubmitEditing={()=> this._onSignUp()}
                  inputRef={ref => this.confirmPassword = ref}
                  ref={ref => this.confirmPasswordInput = ref}
                  style={{width:'90%'}}
                  passwordType={true}
                  returnKeyType="go"
                   />


                  <ButtonComponent 
                  style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:35,marginBottom:19}}
                  handler={this._onSignUp}
                  label ={'Sign Up'}/>

                  
            </View>
      );
     }
  }
  
  
  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
    }
  }
  export default connect(null, mapDispatchToProps)(SignUp);
  
  let styles = StyleSheet.create({
    rootContainer:{
        width:dimensions.SCREEN_WIDTH * 0.9,
        backgroundColor:'white',
        alignItems:'center',
        marginVertical:20,
        padding:10,
        borderRadius:15
      
    },
 
  
  })
  
  