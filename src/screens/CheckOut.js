
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,FlatList} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  import stripe from 'tipsi-stripe'
  import { requestOneTimePayment } from 'react-native-paypal';

  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions,values} from '../utils/constants';
  
  //components
  import UnderlineTextInput from '../components/UnderlineTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import Header from '../components/Header';
  import ProgressBar from '../components/ProgressBar';

  
  //redux
  import { connect } from 'react-redux';
  import { addUser ,apiRequest,apiResponse} from '../actions/actions';


  stripe.setOptions({
    publishableKey:values.STRIPE_DEV_KEY,
    //androidPayMode:'production'
  })
 class CheckOut extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        total:0,
        address_id:0,
        order_id:0
        
      }
    }

    async stripe(){
      const options = {
        requiredBillingAddressFields: 'full',
        prefilledInformation: {
          email:'abc@gmail.com',
          billingAddress: {
            name: 'Gunilla Haugeh',
            line1: 'Canary Place',
            line2: '3',
            city: 'Macon',
            state: 'Georgia',
            country: 'US',
            postalCode: '31217',
          },
        },
      }
    
      const token = await stripe.paymentRequestWithCardForm(options)
      var cardId = token.card.cardId
      var tokenId = token.tokenId
      console.log("carddd",JSON.stringify(token))
        this._makePaymentStripe(tokenId)
    }



    _createOrder = (paymentMethod) =>{
      //paymentMethod  --  1=PayPal, 2=Stripe

      
    var result  = this.props.navigation.getParam('result')
    var amount = result.amount
    var address_id = result.address_id
    var shipping = result.shipping
    var tax = result.tax


      var formData = new FormData();
      formData.append('userid',this.props.rootReducer.user.id);
      formData.append('address_id',address_id);
      formData.append('payment_status','success');
      formData.append('shiping_charge',shipping);
      formData.append('tax',tax);
      formData.append('payment_method',paymentMethod);
      formData.append('amount',amount);
    
      this.props.fetch()
      let url = urls.BASE_URL +'api_createorder'
         fetch(url, {
             method: 'POST',
             body: formData
            }).then((response) => response.json())
                 .then( (responseJson) => {
                  this.props.fetchCancel()
                  console.log(responseJson)
                  if (!responseJson.error){

                    var orderId = responseJson.result.id
                    
                        this.setState({order_id:orderId})
                      if(paymentMethod == 1){
                        //paypal
                        this._requestPaypalToken()
                      }
                      if(paymentMethod == 2){
                        //stripe
                        this.stripe()
                      }
                     }else{
  
                         showMessage(responseJson.message)
                      
                    }
                 }).catch((error) => {
                   console.log(error.message)
                           this.props.fetchCancel()
                           showMessage('Try Again')
                 });
    
  
   }

    _stripeApi = (tokenId) =>{
      var formData = new FormData();
      formData.append('userid',this.props.rootReducer.user.id);
    
      this.props.fetch()
      let url = urls.BASE_URL +'api_cart_items'
         fetch(url, {
             method: 'POST',
             body: formData
            }).then((response) => response.json())
                 .then( (responseJson) => {
                  this.props.fetchCancel()
                  console.log(responseJson)
                  if (!responseJson.error){
  
                     
                     }else{
  
                         showMessage('Error is fetching address.')
                      
                    }
                 }).catch((error) => {
                   console.log(error.message)
                           this.props.fetchCancel()
                           showMessage('Try Again')
                 });
    
  
   }

   _makePaymentStripe = (token) =>{
    var result  = this.props.navigation.getParam('result')
    var amount = result.amount
    var address_id = result.address_id


    var formData = new FormData();
    formData.append('userid',this.props.rootReducer.user.id);
    formData.append('token',token);
  //  formData.append('amount',amount);
    formData.append('amount',amount);
    console.log(formData)
    this.props.fetch()
    let url = urls.BASE_URL +'api_charge_card'
       fetch(url, {
           method: 'POST',
           body: formData
          }).then((response) => response.json())
               .then( (responseJson) => {
                this.props.fetchCancel()
                console.log(responseJson)
                if (!responseJson.error){

                  let obj ={'amount':this.state.total}
                  this.props.navigation.navigate("OrderPlaced",{result:obj})
                   }else{

                       showMessage(responseJson.message)
                    
                  }
               }).catch((error) => {
                 console.log(error.message)
                         this.props.fetchCancel()
                         showMessage(error.message)
               });
  

 }


 _makePaymentPaypal = (nonce,payerId) =>{
  var result  = this.props.navigation.getParam('result')
  var amount = result.amount
  var address_id = result.address_id


  var formData = new FormData();
  formData.append('userid',this.props.rootReducer.user.id);
  formData.append('payment_method_nonce',nonce);
 formData.append('amount',amount);
  formData.append('payerId',this.state.order_id);
  console.log('_makePaymentPaypal----',formData)
  this.props.fetch()
  let url = urls.BASE_URL +'api_paypalcheckout'
     fetch(url, {
         method: 'POST',
         body: formData
        }).then((response) => response.json())
             .then( (responseJson) => {
              this.props.fetchCancel()
              console.log('_makePaymentPaypalResponse------------',responseJson)
              if (!responseJson.error){

                let obj ={'amount':this.state.total}
                this.props.navigation.navigate("OrderPlaced",{result:obj})
                 }else{

                     showMessage(responseJson.message)
                  
                }
             }).catch((error) => {
               console.log(error.message)
                       this.props.fetchCancel()
                       showMessage(error.message)
             });


}

 _requestPaypalToken = ()=>{

  this.props.fetch()
  let url = urls.BASE_URL +'gettoken'
     fetch(url, {
         method: 'GET',
        
        }).then((response) => response.json())
             .then( (responseJson) => {
              this.props.fetchCancel()
            
              if (!responseJson.error){

                 this.paypal(responseJson.data.token)
                 }else{

                     showMessage(responseJson.message)
                  
                }
             }).catch((error) => {
               console.log(error.message)
                       this.props.fetchCancel()
                       showMessage(error.message)
             });
 }

 async paypal(token)  {
  var result  = this.props.navigation.getParam('result')
  var amount = result.amount
  const {
    nonce,
    payerId,
    email,
    firstName,
    lastName,
    phone 
} = await requestOneTimePayment(
  token,
  {
    amount: amount.toString(), // required
    // any PayPal supported currency (see here: https://developer.paypal.com/docs/integration/direct/rest/currency-codes/#paypal-account-payments)
    currency: 'USD',
    // any PayPal supported locale (see here: https://braintree.github.io/braintree_ios/Classes/BTPayPalRequest.html#/c:objc(cs)BTPayPalRequest(py)localeCode)
    localeCode: 'en_USD', 
    shippingAddressRequired: false,
    userAction: 'commit', // display 'Pay Now' on the PayPal review page
    // one of 'authorize', 'sale', 'order'. defaults to 'authorize'. see details here: https://developer.paypal.com/docs/api/payments/v1/#payment-create-request-body
    intent: 'authorize', 
  }

);
console.log('nonce----',nonce)
this._makePaymentPaypal(nonce,payerId)

 }



   
    _onPlaced =()=>{
      showMessage("Select Payment Method")
    }

    componentWillMount(){
      var result  = this.props.navigation.getParam('result')
      var amount = result.amount
      var address_id = result.address_id
      this.setState({
        total:amount,
        address_id:address_id
      })
    }
  
      render() {
          
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                     <Header  label={'CheckOut'} {...this.props}/>
                  
                      <KeyboardAwareScrollView
                      showsVerticalScrollIndicator={false}
                      contentContainerStyle={styles.scrollContainer}>

                        <View style={{padding:10,width:dimensions.SCREEN_WIDTH,alignItems:'center'}}>


                        <View style={styles.topContainer}>
                            <Text style={{color:'grey',marginVertical:5}}>Total Price</Text>
                            <Text style={{color:'black',fontWeight:'bold',fontSize:24,marginVertical:5}}>${parseFloat(this.state.total).toFixed(2)}</Text>
                        </View>

                        <View style={styles.paymentMethod}>
                        <TouchableOpacity onPress={()=> {this._createOrder(1)}}
                        style={{marginHorizontal:5,padding:10,backgroundColor:'white',}}>
                            <FastImage 
                            source={require('../assets/paypal.png')}
                            style={styles.paymentImage} 
                            resizeMode={FastImage.resizeMode.cover}/>
                        </TouchableOpacity>
    
                        <TouchableOpacity onPress={()=> this._createOrder(2)}
                        style={{marginHorizontal:5,padding:10,backgroundColor:'white',}}>
                        <FastImage 
                        source={require('../assets/stripe.png')}
                        style={styles.paymentImage} 
                        resizeMode={FastImage.resizeMode.cover}/>
                    </TouchableOpacity>
                        </View>
                        
                       
                       
                        <ButtonComponent 
                        style={{width :dimensions.SCREEN_WIDTH * 0.92,marginTop:40}}
                        handler={this._onPlaced}
                        label ={'Payment'}/>

                        </View>

                    </KeyboardAwareScrollView>
                    { this.props.apiReducer.isFetching && <ProgressBar/> }     
          </SafeAreaView>
        </>
      );
      }
  }

  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(CheckOut);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      alignItems:'center',
      flexGrow:1,
      backgroundColor:colors.PAGE_BACKGROUND   
    },
    topContainer:{
        width:dimensions.SCREEN_WIDTH * 0.95,
        padding:12,
        backgroundColor:'white',
        marginVertical:10
     },
     paymentMethod:{
         flexDirection:'row',
         justifyContent:'space-evenly',
         alignItems:'center',
         marginVertical:15,
         width:'100%'
     },
     paymentImage:{
         height:80,width:80
     },
   
    
   
   
  })
  
  



