
import {Text, View, Image,Dimensions,ToastAndroid,
      StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,FlatList} 
    from 'react-native';
import React from 'react';
import FastImage from 'react-native-fast-image'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Carousel from 'react-native-banner-carousel';

  //utils
import {showMessage} from '../utils/snackmsg';
import {colors,urls,dimensions} from '../utils/constants';
  
  //components
import CustomTextInput from '../components/CustomTextInput';
import ButtonComponent from '../components/ButtonComponent';
import Header from '../components/Header';
import ChangeQuantity from '../components/ChangeQuantity';
import ProgressBar from '../components/ProgressBar';

  //redux
import { connect } from 'react-redux';
import { addUser ,apiRequest,apiResponse} from '../actions/actions';

class ProductDetails extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        product:'',
        quantity:1,
        gallery:[]
      }
    }


    _addToCart =()=>{
      
      
            const quantity = this.quantityInput.getInputValue();
            
    
    
              this.props.fetch()
              var formData = new FormData();
            
             formData.append('userid',this.props.rootReducer.user.id);
             formData.append('qty',quantity);
             formData.append('product_id', this.state.product.id);
             
        
             let url = urls.BASE_URL +'api_add_to_cart'
              console.log(formData)     
             fetch(url, {
                     method: 'POST',
                     body: formData
                    }).then((response) => response.json())
                         .then( (responseJson) => {
                          this.props.fetchCancel()
          
                          if (!responseJson.error){
          
                              showMessage(responseJson.message,false)
                            
                                    
                          }else{
        
                               showMessage(responseJson.message)
                              
                          }
                         }).catch((error) => {
                                   console.log(error.message)
                                   this.props.fetchCancel()
                                   showMessage('Try Again')
          
                         });
            
    
   }

    _fetch = (productID) =>{
    
      this.props.fetch()
      let url = urls.BASE_URL +'api_getproductdetail?product_id='+ productID
            
         fetch(url, {
             method: 'GET',
            }).then((response) => response.json())
                 .then( (responseJson) => {
                  this.props.fetchCancel()
                  if (!responseJson.error){
                  
                        this.setState({
                          product:responseJson.result.product_detail,
                          gallery:responseJson.result.product_gallery,
                        })
                            
                     }else{
  
                         showMessage('Error is fetching product details.')
                      
                    }
                 }).catch((error) => {
                   console.log(error.message)
                           this.props.fetchCancel()
                           showMessage('Try Again')
                 });
    
  
   }

    componentWillMount(){
      
        var result  = this.props.navigation.getParam('result')
        var product = result.product
      
       // this.setState({product:product})
        this._fetch(product.id)
       
      }

      _viewPdf = (url) =>{
    
        let obj ={
          pdf_url :urls.BASE_URL_MEDIA +  url
        }
        this.props.navigation.navigate('ViewPdf',{result:obj})
      }

      renderGallery= (item, index) =>{
        console.log(item)
        return (
         <FastImage 
         source={{uri:urls.BASE_URL_MEDIA + item.image}}
         style={{height:'100%',width:'100%'}} 
         resizeMode={FastImage.resizeMode.cover}/>
        )
      }
   
 
  
      render() {
          const {product} = this.state
         console.log('----------',product)
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                     <Header  label={'Products Details'} {...this.props}/>
                  
                      <KeyboardAwareScrollView
                      showsVerticalScrollIndicator={false}
                      contentContainerStyle={styles.scrollContainer}>

                        <View style={{padding:10,width:dimensions.SCREEN_WIDTH,alignItems:'center'}}>

                        <Text style={{alignSelf:'center',marginVertical:10,fontWeight:'bold',fontSize:17}}>{product.name}</Text>


                        <View style={{width:'100%',height:200,marginTop:10}}>
                          <Carousel
                            autoplay
                            autoplayTimeout={2000}
                            loop
                            pageIndicatorStyle={{backgroundColor:'white'}}
                            activePageIndicatorStyle={{backgroundColor:colors.COLOR_PRIMARY}}
                            index={0}
                            pageSize={dimensions.SCREEN_WIDTH * 0.95}
                          >
                          {this.state.gallery.map((image, index) => this.renderGallery(image, index))}
                        </Carousel>
                  
                        </View>

                        <Text style={{alignSelf:'flex-start',marginVertical:10,fontWeight:'bold',fontSize:17}}>${product.price}</Text>


                        <Text style={{alignSelf:'flex-start',marginVertical:7,color:'grey'}}>{product.description}</Text>

                       <View style={styles.quantityContainer}>
                          <Text style={{alignSelf:'flex-start',marginVertical:10,fontWeight:'bold',fontSize:17}}>Select quantity</Text>
                          <ChangeQuantity  ref={ref => this.quantityInput = ref}/>
                       </View>

                       {
                         product.document 
                         ? <TouchableOpacity onPress={()=> this._viewPdf( product.document )}
                          style={{alignSelf:'flex-start',flexDirection:'row',alignItems:'center'}}> 
                            <FastImage 
                            source={require('../assets/pdf.png')}
                            style={{height:30,width:30}}
                            resizeMode={FastImage.resizeMode.cover}/>
                           <Text style={{margin:10,fontWeight:'bold',fontSize:17,color:colors.COLOR_PRIMARY}}>Documents</Text>
                          </TouchableOpacity>
                         : null
                       }
                       

                        <ButtonComponent 
                          style={{width :dimensions.SCREEN_WIDTH * 0.92,marginTop:40}}
                         handler={this._addToCart}
                          label ={'Add to cart'}/>

                        </View>
                    </KeyboardAwareScrollView>
                    { this.props.apiReducer.isFetching && <ProgressBar/> }   
          </SafeAreaView>
        </>
      );
      }
  }

  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      alignItems:'center',
      flexGrow:1,
      backgroundColor:colors.PAGE_BACKGROUND   
    },
    quantityContainer:{flexDirection:'row',
    justifyContent: 'space-between',width:'100%',
       alignItems:'center'}
   
   
  })
  
  