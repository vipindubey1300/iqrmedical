
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,FlatList} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  import Carousel from 'react-native-banner-carousel';


  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import CustomTextInput from '../components/CustomTextInput';
  import ProgressBar from '../components/ProgressBar';
  import Header from '../components/Header';
  import SearchHeader from '../components/SearchHeader';
  import CategoriesComponent from '../components/CategoriesComponent';
  import ProductsComponent from '../components/ProductsComponent';

  //redux
import { connect } from 'react-redux';
import { addUser ,apiRequest,apiResponse} from '../actions/actions';






 class Home extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
         loading_status:false,
         sliders:[],
         categories:[],
          products:[],
         home_static:[ {
          "id": 1,
          "name": "Home Page",
          "image": "siteimages/sliderimg/user1588225686.png",
          "heading": "Your Health,We Care",
          "short_description": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium "
      }]
      }
    }

    _fetch = () =>{
    
      this.props.fetch()
      let url = urls.BASE_URL +'api_gethomedata'
            
         fetch(url, {
             method: 'GET',
            }).then((response) => response.json())
                 .then( (responseJson) => {
                  this.props.fetchCancel()
  
                  if (!responseJson.error){
  
                  
                        this.setState({
                          sliders:responseJson.result.slider,
                          categories:responseJson.result.productcategory,
                          products:responseJson.result.product,
                          home_static:responseJson.result.homestaticcontent,
                        })
                            
                     }else{
  
                         showMessage(responseJson.message)
                      
                    }
                 }).catch((error) => {
                   console.log(error.message)
                   this.props.fetchCancel()
                           showMessage('Try Again')
  
                 });
    
  
   }


   _fetchCategoryProducts = (catId) =>{
    
    this.props.fetch()
    let url = urls.BASE_URL +'api_getproductbycategory?category_id='+catId
          
       fetch(url, {
           method: 'GET',
          }).then((response) => response.json())
               .then( (responseJson) => {
                this.props.fetchCancel()

                if (!responseJson.error ){
                  if (responseJson.result.length > 0 ){
                      let obj ={
                        'products': responseJson.result
                      }
                      this.props.navigation.navigate("ShowProducts",{result:obj})
                
                  }
                
                 }else{

                       showMessage(responseJson.message)
                    
                  }
               }).catch((error) => {
                 console.log(error.message)
                 this.props.fetchCancel()
                         showMessage('Try Again')

               });
  

 }

   componentWillMount(){
     this._fetch()
   }

    onCategoriesClick = (item) =>{
      console.log('onCategoriesClick--------',JSON.stringify(item))
      var cat_id = item.id
      this._fetchCategoryProducts(cat_id)
    }
    
    onProductsClick = (item) =>{
      console.log('onProductsClick--------',JSON.stringify(item))
      let obj={'product':item}
      this.props.navigation.navigate('ProductDetails',{result:obj})
    }


    _renderItemCategories = ({item, index}) =>{
      //console.log()
     return (
        <CategoriesComponent
         object = {item} 
         index ={index}
         clickHandler ={this.onCategoriesClick}/>
     )
   }

   _renderItemProducts = ({item, index}) =>{
    console.log()
   return (
      <ProductsComponent
       object = {item} 
       index ={index}
       clickHandler ={this.onProductsClick}/>
   )
 }


    _allProducts = () =>{
      this.props.navigation.navigate('Products')
    }

    renderSliders= (item, index) =>{
     console.log(item)
     return (
      <FastImage 
      source={{uri:urls.BASE_URL_MEDIA + item.image}}
      style={{height:'100%',width:'100%'}} 
      resizeMode={FastImage.resizeMode.cover}/>
     )
   }


   _onSearch =(result)=>{
    let obj ={
      'products':result
    }
    this.props.navigation.navigate("ShowProducts",{result:obj})
   }
   
  
    
  
      render() {
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                     <Header haveMenu={true} label={'Home'} {...this.props}/>
                  
                      <KeyboardAwareScrollView
                      showsVerticalScrollIndicator={false}
                      contentContainerStyle={styles.scrollContainer}>
                      <SearchHeader style={styles.searchHeader} 
                      showProduct={this._onSearch}
                      {...this.props}/>

                        <View style={{padding:10,width:dimensions.SCREEN_WIDTH}}>

                        <View style={{width:'100%',height:200,marginTop:-100}}>

                         <Carousel
                            autoplay
                            autoplayTimeout={2000}
                            loop
                            pageIndicatorStyle={{backgroundColor:'white'}}
                            activePageIndicatorStyle={{backgroundColor:colors.COLOR_PRIMARY}}
                            index={0}
                            pageSize={dimensions.SCREEN_WIDTH * 0.95}
                          >
                           {this.state.sliders.map((image, index) => this.renderSliders(image, index))}
                        </Carousel>
                      
                        </View>

                        <Text style={styles.label}>Categories</Text>

                        <View style={styles.categoriesContainer}>
                          <FlatList
                            style={{marginVertical:10}}
                            horizontal={true}
                            data={this.state.categories}
                            showsHorizontalScrollIndicator={false}
                            renderItem={(item,index) => this._renderItemCategories(item,index)}
                            keyExtractor={item => item.id}
                          />
                        </View>


                        <View style={{width:'100%',height:130,marginTop:15}}>
                        <FastImage 
                        source={{uri:urls.BASE_URL_MEDIA + this.state.home_static[0].image}}
                        style={{height:'100%',width:'100%'}} 
                        resizeMode={FastImage.resizeMode.stretch}/>
                        </View>


                        <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginVertical:7}}>
                          <Text style={styles.label}>Products</Text>
                          <TouchableOpacity  onPress={()=> this._allProducts()}>
                          <Text style={{color:colors.COLOR_PRIMARY,fontSize:14}}>View all</Text>
                          </TouchableOpacity>
                          
                        </View>

                            <FlatList
                            style={{marginVertical:10}}
                            data={this.state.products}
                            showsVerticalScrollIndicator={false}
                            renderItem={(item,index) => this._renderItemProducts(item,index)}
                            keyExtractor={item => item.id}
                          />



                        </View>


                        
                      
                    
                    </KeyboardAwareScrollView>
                    { this.props.apiReducer.isFetching && <ProgressBar/> }     
          </SafeAreaView>
        </>
      );
      }
  }

  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(Home);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      alignItems:'center',
      flexGrow:1,
      backgroundColor:colors.PAGE_BACKGROUND   
    },
    searchHeader:{
        height :160,
        borderBottomRightRadius:10,
        borderBottomLeftRadius:10,
        backgroundColor:'white',
        overflow:'hidden',
        elevation:0
    },
    categoriesContainer:{
      padding:10,
      backgroundColor:'white',
      elevation:2,
      marginVertical:10,
      shadowColor: '#000000',
      shadowOffset: { width: 1, height: 1 },
      shadowOpacity:  0.2,
    },
    label:{marginTop:15,color:'grey',fontSize:18,fontWeight:'bold'}
   
  })
  
  