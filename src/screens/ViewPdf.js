
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  
  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import CustomTextInput from '../components/CustomTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import LoginHeader from '../components/LoginHeader';
  import Header from '../components/Header';

  import Pdf from 'react-native-pdf';

  
  export default class ViewPdf extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        pdf_url:''
      }
    }


  componentWillMount(){

    let result = this.props.navigation.getParam('result')
    var url = result['pdf_url']
    this.setState({pdf_url:url})
    console.log('urrrrrllllllllllllllll',url)
  
  }

    
  
      render() {
        const source = {uri:this.state.pdf_url,cache:true};

       return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
        <SafeAreaView style={styles.container}>
        <Header  label={'PDF'} {...this.props}/>
     
        <View style={{backgroundColor:'black',width:'100%',
        flex:1,justifyContent:'center',alignItems:'center'}}>

        {
            source == null ?
            
             null
              :
             <Pdf
             source={source}
             ref={(pdf) => { this.pdf = pdf; }}
             onLoadComplete={(numberOfPages,filePath)=>{
                 console.log(`number of pages: ${numberOfPages}`);
             }}
             onPageChanged={(page,numberOfPages)=>{
                 console.log(`current page: ${page}`);
             }}
             onError={(error)=>{
                 console.log(error);
             }}
             onPressLink={(uri)=>{
                 console.log(`Link presse: ${uri}`)
             }}
             style={styles.pdf}/>

        }

           
          
          </View>
       { this.state.loading_status && <ProgressBar/> }     
    </SafeAreaView>
        </>
      );
      }
  }
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      padding:10,
      alignItems:'center',
      flexGrow:1,
      backgroundColor:'white'
    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
    }
   
  })
  
  