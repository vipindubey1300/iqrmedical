
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,TextInput} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  
  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import CustomTextInput from '../components/CustomTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import LoginHeader from '../components/LoginHeader';
  import ProgressBar from '../components/ProgressBar';

  
  
  export default class Otp extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        otp1:'',
        otp2:'',
        otp3:'',
        otp4:'',
        otp5:'',
        otp6:'',
        
      }
    }
    
  
    _isValid() {


      
      var isnumotp1 = /^\d+$/.test(this.state.otp1);
      var isnumotp2 = /^\d+$/.test(this.state.otp2);
      var isnumotp3 = /^\d+$/.test(this.state.otp3);
      var isnumotp4 = /^\d+$/.test(this.state.otp4);
      var isnumotp5 = /^\d+$/.test(this.state.otp5);
      var isnumotp6 = /^\d+$/.test(this.state.otp6);

      let valid = false;

      if (this.state.otp1.trim().length > 0 &&
         this.state.otp2.trim().length > 0 &&
         this.state.otp3.trim().length > 0 &&
         this.state.otp4.trim().length > 0 &&
         this.state.otp5.trim().length > 0 &&
         this.state.otp6.trim().length > 0  ){
           valid = true;
           return true;
      }
     
     if(this.state.otp1.toString().trim().length === 0
     || this.state.otp2.toString().trim().length === 0
     || this.state.otp3.toString().trim().length === 0
     || this.state.otp4.toString().trim().length === 0
     || this.state.otp5.toString().trim().length === 0
     || this.state.otp6.toString().trim().length === 0){


        showMessage('Enter Valid OTP')
        return false;

      } 
     

      //return valid;
  }


    // _onOtp =()=>{
    //     this.props.navigation.navigate("ResetPassword")
    // }
    
    _onOtp = () =>{
      if(this._isValid()){
  
        Keyboard.dismiss()
  
  
  
        const otp = this.state.otp1 + this.state.otp2 + this.state.otp3 + this.state.otp4  + this.state.otp5  + this.state.otp6
  
  
        var result  = this.props.navigation.getParam('result')
        var email = result.email
      //var otp1 = result.otp
    
        this.setState({loading_status:true})
        var formData = new FormData();
      
       formData.append('otp',otp);
       formData.append('email',email);
  
        
    
        let url = urls.BASE_URL +'api_verifyotp_resetpassword'
              console.log("FFF",JSON.stringify(url))
               fetch(url, {
               method: 'POST',
               body: formData
              }).then((response) => response.json())
                   .then( (responseJson) => {
                       this.setState({loading_status:false})
    
                       console.log("FFF",JSON.stringify(responseJson))
  
    
                    if (!responseJson.error){
                      showMessage('OTP Verified Successfully.',false)
  
                      this.setState({
                        otp1:'',otp2:'',otp3:'',otp4:'',otp5:'',otp6:''
                      })
                      var id = responseJson.result.userid
                     
  
                        let tempObject = {
                          'email' : email,
                          'id' : id,
                         }
                       this.props.navigation.navigate("ResetPassword",{result:tempObject})
  
                      
                      
                              
                       }else{
                         this.setState({
                           otp1:'',otp2:'',otp3:'',otp4:'',otp5:'',otp6:''
                         })
                           showMessage(responseJson.message)
                        
                        }
                   }).catch((error) => {
                     console.log(error)
                             this.setState({loading_status:false})
                             showMessage(error.message)
    
                   });
      }
    
     }
  
      render() {
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                  {/* position absolute make the image to go in safe are so wrapping in view solve this issue */}
                  {/* image background */}
                  <View>
                    <Image source={require('../assets/signin-bg.jpg')} 
                      resizeMode="stretch"
                      style={styles.imageContainer}/>
                   </View>
                  {/* image background  end*/}

                  <LoginHeader {...this.props}/>
  
  
                      <KeyboardAwareScrollView
                      contentContainerStyle={styles.scrollContainer}>
  
                      <FastImage 
                        source={require('../assets/logo.png')}
                        style={styles.headerLogo} 
                        resizeMode={FastImage.resizeMode.contain}/>

                        <Text style={styles.headingText}>VERIFICATION CODE</Text>

                        <View style={styles.rootContainer}>
                        <Text style={{
                            color:'grey',
                            fontSize:15,
                            textAlign:'center',marginHorizontal:20,marginTop:10
                        }}>Verification code has sent to your registered email ID.Please enter OTP to reset your password</Text>



                        <View style={{flexDirection:'row',width:'100%'}}>

                            <TextInput
                            value={this.state.otp1}
                            maxLength={1}
                            onChangeText={ (otp1) => {
                            otp1.length ==1 ? this.otp2.focus() : null
                            this.setState({otp1:otp1.replace(/[^0-9.]/g, '')})
                            }}
                        // ref='otp1'
                            ref={(input) => { this.otp1 = input; }}
                            autoCorrect={false}
                            autoCapitalize={'none'}
                            keyboardType = 'numeric'
                            textContentType='telephoneNumber'
                            style={styles.input}
                            placeholderTextColor={'grey'}
                        />
          
                        <TextInput
                        value={this.state.otp2}
                        maxLength={1}
                        ref={(input) => { this.otp2 = input; }}
                        
                        onChangeText={ (otp2) => {
                            otp2.length ==1 ? this.otp3.focus() : null
                            this.setState({otp2:otp2.replace(/[^0-9.]/g, '')})
                        }}
                        onKeyPress={e =>{ 
                                    if(e.nativeEvent.key == 'Backspace'){
                                        this.otp1.focus();
                                    this.setState({otp2:''})
                                    }
                            }} 
                        
                        autoCorrect={false}
                        autoCapitalize={'none'}
                        keyboardType = 'numeric'
                        textContentType='telephoneNumber'
                        style={styles.input}
                        placeholderTextColor={'grey'}
                        />
                        <TextInput
                        value={this.state.otp3}
                        maxLength={1}
                        ref={(input) => { this.otp3 = input; }}
                        onChangeText={ (otp3) => {
                            otp3.length ==1 ? this.otp4.focus() : null
                            this.setState({otp3:otp3.replace(/[^0-9.]/g, '')})
                        }}
                        
                        autoCorrect={false}
                        autoCapitalize={'none'}
                        keyboardType = 'numeric'
                        textContentType='telephoneNumber'
                        style={styles.input}
                        placeholderTextColor={'grey'}
                        onKeyPress={e =>{ 
                                    if(e.nativeEvent.key == 'Backspace'){
                                        this.otp2.focus();
                                    this.setState({otp3:''})
                                    }
                                    }} 
                        />
          
                        <TextInput
                        value={this.state.otp4}
                        maxLength={1}
                        ref={(input) => { this.otp4 = input; }}
                        onChangeText={ (otp4) => {

                          otp4.length ==1 ? this.otp5.focus() : null
                           this.setState({otp4:otp4.replace(/[^0-9.]/g, '')})
                         
                        // var a = otp4.replace(/[^0-9.]/g, '')
                        // this.setState({otp4:a},()=>{
                        //   otp4.length ==1 ? this._onOtp() : null
                        // })
                        }}
                        onKeyPress={e =>{ 
                                    if(e.nativeEvent.key == 'Backspace'){
                                        this.otp3.focus();
                                    this.setState({otp4:''})
                                    }
                                    }} 
                        autoCorrect={false}
                        autoCapitalize={'none'}
                        keyboardType = 'numeric'
                        textContentType='telephoneNumber'
                        style={styles.input}
                       
                        placeholderTextColor={'grey'}
                        returnKeyType='go'

                    />

                    <TextInput
                        value={this.state.otp5}
                        maxLength={1}
                        ref={(input) => { this.otp5 = input; }}
                        onChangeText={ (otp5) => {
                         
                          otp5.length ==1 ? this.otp6.focus() : null
                           this.setState({otp5:otp5.replace(/[^0-9.]/g, '')})
                        }}
                        onKeyPress={e =>{ 
                                    if(e.nativeEvent.key == 'Backspace'){
                                        this.otp4.focus();
                                    this.setState({otp5:''})
                                    }
                                    }} 
                        autoCorrect={false}
                        autoCapitalize={'none'}
                        keyboardType = 'numeric'
                        textContentType='telephoneNumber'
                        style={styles.input}
                        onSubmitEditing={()=> this._onOtp()}
                        placeholderTextColor={'grey'}
                        returnKeyType='go'

                    />


                    <TextInput
                    value={this.state.otp6}
                    maxLength={1}
                    ref={(input) => { this.otp6 = input; }}
                    onChangeText={ (otp6) => {
   
                    var a = otp6.replace(/[^0-9.]/g, '')
                    this.setState({otp6:a},()=>{
                      otp6.length ==1 ? this._onOtp() : null
                    })
                    }}
                    onKeyPress={e =>{ 
                                if(e.nativeEvent.key == 'Backspace'){
                                      this.otp5.focus();
                                     this.setState({otp6:''})
                                }
                                }} 
                    autoCorrect={false}
                    autoCapitalize={'none'}
                    keyboardType = 'numeric'
                    textContentType='telephoneNumber'
                    style={styles.input}
                    onSubmitEditing={()=> this._onOtp()}
                    placeholderTextColor={'grey'}
                    returnKeyType='go'

                />
                
          
                  </View>
  
                        <ButtonComponent 
                        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:40}}
                        handler={this._onOtp}
                        label ={'Next'}/>

                      </View>
                 
                    
                    </KeyboardAwareScrollView>
  
                    { this.state.loading_status && <ProgressBar/> }
               
          </SafeAreaView>
        </>
      );
      }
  }
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    imageContainer:{
        position: 'absolute',
        flex: 1,
        backgroundColor:'rgba(0,0,0,0.45)',
        width:dimensions.SCREEN_WIDTH,
        height: dimensions.SCREEN_HEIGHT
    },
  
    scrollContainer:{
      padding:10,
      alignItems:'center'
    },
    headerLogo:{
        height: 120, 
        width:dimensions.SCREEN_WIDTH * 0.6, 
        margin:10
  
    },
    headingText:{
      color:colors.BLACK,
      fontSize:18,
      marginVertical:25,
      fontWeight:'bold',
      textAlign:'center',
      color:'white'
    },
    rootContainer:{
        width:dimensions.SCREEN_WIDTH * 0.9,
        backgroundColor:'white',
        alignItems:'center',
        marginVertical:20,
        padding:10,
        borderRadius:15
      
    },
    input: {
        width: "100%",
        height: 46,
        textAlign: 'center',
        borderRadius: 0,
        borderBottomWidth: 2,
        backgroundColor: colors.WHITE,
        borderBottomColor: colors.GREY,
        flex:1,
        margin:10,
        color:colors.BLACK
      },
  })
  
  