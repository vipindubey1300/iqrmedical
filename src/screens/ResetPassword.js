
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  
  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import CustomTextInput from '../components/CustomTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import LoginHeader from '../components/LoginHeader';
  import ProgressBar from '../components/ProgressBar';

  
  
  export default class ResetPassword extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false
      }
    }
    
  

    _isValid = () =>{

      const newPassword = this.passwordInput.getInputValue();
      const confirmPassword = this.confirmPasswordInput.getInputValue();

      if(newPassword.trim().length == 0){
        showMessage('Enter New Password')
        return false
    
      }
      else if (newPassword.trim().length < 8 || newPassword.trim().length > 16) {
        showMessage('New Password should be 8-16 characters long')
        return false;
      }
      else if(confirmPassword.trim().length == 0){
        showMessage('Enter Confirm Password')
        return false
    
      }
      else if (confirmPassword.trim().length < 8 || confirmPassword.trim().length > 16) {
        showMessage('Confirm Password should be 8-16 characters long')
        return false;
      }
      else if(confirmPassword !== newPassword ){
        showMessage('New Password and Confirm Password Should match')
        return false
    
      }
    
      else{
        return true;
      }
    
    }
    
    
    _resetPassword =() =>{
    
    if(this._isValid()){
      const newPassword = this.passwordInput.getInputValue();
      const confirmPassword = this.confirmPasswordInput.getInputValue();
    
      this.setState({loading_status:true})
        var formData = new FormData();
        Keyboard.dismiss()

        var result  = this.props.navigation.getParam('result')
        var id = result.id
      
    
         formData.append('userid',id);
         formData.append('newpass',newPassword);
         formData.append('cpass',confirmPassword);
         formData.append('device_token', 'jaBBD87dg7D');
          Platform.OS =='android'
          ?  formData.append('device_type',1)
          : formData.append('device_type',2)
       
               let url = urls.BASE_URL +'api_resetpassword'
               fetch(url, {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'multipart/form-data',
               },
               body: formData
              }).then((response) => response.json())
                   .then( async(responseJson) => {
                       this.setState({loading_status:false})
    
    
                      console.log(JSON.stringify(responseJson))
                    if (!responseJson.error){
                      try {
                       showMessage(responseJson.message,false)
                        this.props.navigation.navigate("Login")
                        
                        return true;
                      }
                      catch(exception) {
                        
                        return false;
                      }
    
    
                        
                       }else{
                             showMessage(responseJson.message)
                      }
                   }).catch((error) => {
                             this.setState({loading_status:false})
                             showMessage('Try Again.')
                        
                   });
    }
    
    }

    componentDidMount(){
      var result  = this.props.navigation.getParam('result')
      var email = result.email

      this.emailInput.setState({text:email})
    }
  
  
      render() {
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                  {/* position absolute make the image to go in safe are so wrapping in view solve this issue */}
                  {/* image background */}
                  <View>
                    <Image source={require('../assets/signin-bg.jpg')} 
                      resizeMode="stretch"
                      style={styles.imageContainer}/>
                   </View>
                  {/* image background  end*/}

                  <LoginHeader {...this.props}/>
  
  
                      <KeyboardAwareScrollView
                      contentContainerStyle={styles.scrollContainer}>
  
                      <FastImage 
                        source={require('../assets/logo.png')}
                        style={styles.headerLogo} 
                        resizeMode={FastImage.resizeMode.contain}/>

                        <Text style={styles.headingText}>RESET PASSWORD</Text>

                        <View style={styles.rootContainer}>
                        <CustomTextInput
                            placeholder={'Email'}
                            inputRef={ref => this.email = ref}
                            ref={ref => this.emailInput = ref}
                            style={{width:'90%'}}
                            editable={false}
                            returnKeyType="next"
                          />
                      
                          <CustomTextInput
                          placeholder={'Password'}
                          onSubmitEditing={()=> this.confirmPassword.focus()}
                          inputRef={ref => this.password = ref}
                          ref={ref => this.passwordInput = ref}
                          style={{width:'90%'}}
                          passwordType={true}
                          returnKeyType="next"
                        />
                    
                        <CustomTextInput
                        placeholder={'Confirm Password'}
                        onSubmitEditing={()=> this._resetPassword()}
                        inputRef={ref => this.confirmPassword = ref}
                        ref={ref => this.confirmPasswordInput = ref}
                        passwordType={true}
                        style={{width:'90%'}}
                        returnKeyType="go"
                      />
                  
  
                        <ButtonComponent 
                        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:40}}
                        handler={this._onForgot}
                        label ={'Next'}/>

                      </View>
                 
                    
                    </KeyboardAwareScrollView>
  
                    { this.state.loading_status && <ProgressBar/> }
               
          </SafeAreaView>
        </>
      );
      }
  }
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    imageContainer:{
        position: 'absolute',
        flex: 1,
        backgroundColor:'rgba(0,0,0,0.45)',
        width:dimensions.SCREEN_WIDTH,
        height: dimensions.SCREEN_HEIGHT
    },
  
    scrollContainer:{
      padding:10,
      alignItems:'center'
    },
    headerLogo:{
        height: 120, 
        width:dimensions.SCREEN_WIDTH * 0.6, 
        margin:10
  
    },
    headingText:{
      color:colors.BLACK,
      fontSize:18,
      marginVertical:25,
      fontWeight:'bold',
      textAlign:'center',
      color:'white'
    },
    rootContainer:{
        width:dimensions.SCREEN_WIDTH * 0.9,
        backgroundColor:'white',
        alignItems:'center',
        marginVertical:20,
        padding:10,
        borderRadius:15
      
    },
  })
  
  