
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,FlatList} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  import AsyncStorage from '@react-native-community/async-storage';


  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import CustomTextInput from '../components/CustomTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import Header from '../components/Header';
  import ProgressBar from '../components/ProgressBar';

  
  //redux
  import { connect } from 'react-redux';
  import { addUser ,apiRequest,apiResponse,removeUser} from '../actions/actions';

 class ChangePassword extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        
      }
    }

    remove = async() =>{
      try {
       
        this.props.remove({});
    
        await AsyncStorage.removeItem("id");
        await AsyncStorage.removeItem("name");
        await AsyncStorage.removeItem("image");
        await AsyncStorage.removeItem("email");
        await AsyncStorage.removeItem("phone");
        
       this.props.navigation.navigate("Login")
      
        return true;
      }
      catch(exception) {
        
        return false;
      }
    }


    _isValid = () =>{
      const oldPassword = this.oldPasswordInput.getInputValue();
      const newPassword = this.newPasswordInput.getInputValue();
      const confirmPassword = this.confirmPasswordInput.getInputValue();

      if(oldPassword.trim().length == 0){
        showMessage('Enter Old Password')
        return false
    
      }
      else if(newPassword.trim().length == 0){
        showMessage('Enter New Password')
        return false
    
      }
      else if (newPassword.trim().length < 8 || newPassword.trim().length > 16) {
        showMessage('New Password should be 8-16 characters long')
        return false;
      }
      else if(confirmPassword.trim().length == 0){
        showMessage('Enter Confirm Password')
        return false
    
      }
      else if (confirmPassword.trim().length < 8 || confirmPassword.trim().length > 16) {
        showMessage('Confirm Password should be 8-16 characters long')
        return false;
      }
      else if(confirmPassword !== newPassword ){
        showMessage('New Password and Confirm Password Should match')
        return false
    
      }
    
      else{
        return true;
      }
    
    }
    
    
    _onChangePassword =() =>{
    
    if(this._isValid()){
      const oldPassword = this.oldPasswordInput.getInputValue();
      const newPassword = this.newPasswordInput.getInputValue();
      const confirmPassword = this.confirmPasswordInput.getInputValue();
    
      this.props.fetch()
        var formData = new FormData();
        Keyboard.dismiss()

       
    
         formData.append('userid',this.props.rootReducer.user.id);
         formData.append('oldpass',oldPassword);
         formData.append('newpass',newPassword);
         formData.append('cpass',confirmPassword);
         formData.append('device_token', 'jaBBD87dg7D');
          Platform.OS =='android'
          ?  formData.append('device_type',1)
          : formData.append('device_type',2)
       
               let url = urls.BASE_URL +'api_changepassword'
               fetch(url, {
               method: 'POST',
               body: formData
              }).then((response) => response.json())
                   .then((responseJson) => {
                    this.props.fetchCancel()
    
    
                      console.log(JSON.stringify(responseJson))
                    if (!responseJson.error){
                      try {
                        this.remove()
                        showMessage(responseJson.message,false)
                        
                        
                        return true;
                      }
                      catch(exception) {
                        
                        return false;
                      }
    
    
                        
                       }else{
                             showMessage(responseJson.message)
                      }
                   }).catch((error) => {
                     console.log(error)
                              this.props.fetchCancel()
                             showMessage(error.message)
                        
                   });
    }
    
    }
  
      render() {
          
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                     <Header  label={'Change Password'} {...this.props}/>
                  
                      <KeyboardAwareScrollView
                      showsVerticalScrollIndicator={false}
                      contentContainerStyle={styles.scrollContainer}>

                        <View style={{padding:10,width:dimensions.SCREEN_WIDTH,alignItems:'center'}}>

                        <CustomTextInput
                        placeholder={'Old Password'}
                        secureTextEntry={true}
                        onSubmitEditing={()=> this.newPassword.focus()}
                        inputRef={ref => this.oldPassword = ref}
                        ref={ref => this.oldPasswordInput = ref}
                        style={{width:'90%',backgroundColor:colors.PAGE_BACKGROUND}}
                        passwordType={true}
                        returnKeyType="next"
                         />

                        <CustomTextInput
                        placeholder={'New Password'}
                        secureTextEntry={true}
                        onSubmitEditing={()=> this.confirmPassword.focus()}
                        inputRef={ref => this.newPassword = ref}
                        ref={ref => this.newPasswordInput = ref}
                        style={{width:'90%',backgroundColor:colors.PAGE_BACKGROUND}}
                        passwordType={true}
                        returnKeyType="next"
                         />
       
       
                         <CustomTextInput
                         placeholder={'Confirm Password'}
                         secureTextEntry={true}
                         onSubmitEditing={()=> this._onChangePassword()}
                         inputRef={ref => this.confirmPassword = ref}
                         ref={ref => this.confirmPasswordInput = ref}
                         style={{width:'90%',backgroundColor:colors.PAGE_BACKGROUND}}
                         passwordType={true}
                         returnKeyType="go"
                          />
       
                       
                        <ButtonComponent 
                        style={{width :dimensions.SCREEN_WIDTH * 0.92,marginTop:40}}
                        handler={this._onChangePassword}
                        label ={'Submit'}/>

                        </View>
                    </KeyboardAwareScrollView>
                    { this.props.apiReducer.isFetching && <ProgressBar/> }     
          </SafeAreaView>
        </>
      );
      }
  }

  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
        remove : (userinfo) => dispatch(removeUser(userinfo)),  
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      alignItems:'center',
      flexGrow:1,
      backgroundColor:colors.PAGE_BACKGROUND   
    },
   
   
  })
  
  