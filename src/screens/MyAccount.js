
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,FlatList} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  import Carousel from 'react-native-banner-carousel';

  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import CustomTextInput from '../components/CustomTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import Header from '../components/Header';
  import ChangeQuantity from '../components/ChangeQuantity';
  import ProgressBar from '../components/ProgressBar';

  //redux
  import { connect } from 'react-redux';
  import { addUser ,apiRequest,apiResponse} from '../actions/actions';
  class MyAccount extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        profile_details:''
      }
    }

    _fetch = () =>{
    
      this.props.fetch()
      let url = urls.BASE_URL +'api_getprofile?userid='+ this.props.rootReducer.user.id
            
         fetch(url, {
             method: 'GET',
            }).then((response) => response.json())
                 .then( (responseJson) => {
                  this.props.fetchCancel()
                  console.log(responseJson)
                  if (!responseJson.error){
                  
                        this.setState({profile_details:responseJson.result})
                            
                     }else{
  
                         showMessage('Error is fetching product details.')
                      
                    }
                 }).catch((error) => {
                   console.log(error.message)
                           this.props.fetchCancel()
                           showMessage('Try Again')
                 });
    
  
   }

    componentWillMount(){
        this._fetch() 
    }

    _editProfile =()=>{
      this.props.navigation.navigate("EditProfile")
    }

    _changePassword =()=>{
      this.props.navigation.navigate("ChangePassword")
    }

    _myAddress =()=>{
      this.props.navigation.navigate("MyAddress")
    }



    
  _myOrder =()=>{
    this.props.navigation.navigate("MyOrders")
  }
      render() {
        const {profile_details} = this.state
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                     <Header  label={'Profile'} {...this.props}/>
                  
                      <KeyboardAwareScrollView
                      showsVerticalScrollIndicator={false}
                      contentContainerStyle={styles.scrollContainer}>

                      <View style={styles.topContainer}>
                        <Text style={{textTransform:'capitalize',fontWeight:'bold',fontSize:19}}>{profile_details.name}</Text>
                        <Text style={{marginVertical:7}}>{profile_details.email}</Text>

                        <TouchableOpacity style={styles.editButton}
                        onPress={()=> this._editProfile()}>
                        <Text style={{textTransform:'capitalize'}}>Edit Profile</Text>
                        </TouchableOpacity>
                      </View>

                      <View style={styles.bottomContainer}>

                      <Text style={{textTransform:'capitalize',fontSize:15,marginLeft:10}}>{profile_details.name}</Text>
                      <View style={styles.divider}></View>

                      <Text style={{fontSize:15,marginLeft:10}}>{profile_details.email}</Text>
                      <View style={styles.divider}></View>

                      <Text style={{textTransform:'capitalize',fontSize:15,marginLeft:10}}>{profile_details.mobile}</Text>
                      <View style={styles.divider}></View>

                      <Text onPress={()=> this._changePassword()}
                      style={{textTransform:'capitalize',fontSize:15,marginLeft:10}}>Reset Password</Text>
                      <View style={styles.divider}></View>

                      <Text onPress={()=> this._myAddress()}
                      style={{textTransform:'capitalize',fontSize:15,marginLeft:10}}>Manage Address</Text>
                      <View style={styles.divider}></View>

                      </View>



                      

                        <ButtonComponent 
                          style={{width :dimensions.SCREEN_WIDTH * 0.92,marginTop:70}}
                          handler={this._myOrder}
                          label ={'My Orders'}/>

                    
                    </KeyboardAwareScrollView>
                    { this.props.apiReducer.isFetching && <ProgressBar/> }   
          </SafeAreaView>
        </>
      );
      }
  }

  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(MyAccount);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      alignItems:'center',
      flexGrow:1,
      backgroundColor:colors.PAGE_BACKGROUND   
    },
    topContainer:{
         justifyContent: 'center',
         width:'100%',
        alignItems:'center',
        paddingVertical:20
    },
    editButton:{
      marginVertical:19,
      borderRadius:8,
      borderWidth:1,
      borderColor:'grey',
      paddingHorizontal:13,
      paddingVertical:8,
      justifyContent:'center',
      alignItems:'center'

    },
    bottomContainer:{
      paddingHorizontal:10,
      paddingVertical:16,
      backgroundColor:colors.WHITE,
      width:dimensions.SCREEN_WIDTH * 0.95
    },
    divider:{
      height:1,
      width:'100%',
      marginVertical:10,
      backgroundColor:'grey'
    }
   
   
  })
  
  