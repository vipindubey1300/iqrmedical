
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,FlatList} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  
  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import CustomTextInput from '../components/CustomTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import Header from '../components/Header';
  import SearchHeader from '../components/SearchHeader';
  import MyOrdersComponent from '../components/MyOrdersComponent';
  import ProgressBar from '../components/ProgressBar';

  //redux
  import { connect } from 'react-redux';
  import { addUser ,apiRequest,apiResponse} from '../actions/actions';


 const products = [
  {
      "id" :'1',
      "name": "Surgical Gowns",
      'description':"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "image": require('../assets/img4.png'),
      'date':'31 Monday 2020 | 12:00'
  },
  {
    "id" :'1',
    "name": "Surgical Gowns",
    'description':"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    "image": require('../assets/img5.png'),
    'date':'31 Monday 2020 | 12:00'
},
{
    "id" :'1',
    "name": "Surgical Gowns",
    'description':"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    "image": require('../assets/img1.png'),
    'date':'31 Monday 2020 | 12:00'
},
{
  "id" :'1',
  "name": "Surgical Gowns",
  'description':"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
  "image": require('../assets/img2.png'),
  'date':'31 Monday 2020 | 12:00'
},
 {
      "id" :'1',
      "name": "Surgical Gowns",
      'description':"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "image": require('../assets/img4.png'),
      'date':'31 Monday 2020 | 12:00'
  },
  {
    "id" :'1',
    "name": "Surgical Gowns",
    'description':"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    "image": require('../assets/img.png'),
    'date':'31 Monday 2020 | 12:00'
},

]
  
class MyOrders extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        orders:[]
      }
    }

    _fetchMyOrders = () =>{
    
      this.props.fetch()
      let url = urls.BASE_URL +'api_getmyorderlist?userid='+ this.props.rootReducer.user.id
     // let url = urls.BASE_URL +'api_getmyorderlist?userid=1'

            
         fetch(url, {
             method: 'GET',
            }).then((response) => response.json())
                 .then( (responseJson) => {
                  this.props.fetchCancel()
                  console.log(responseJson)
                  if (!responseJson.error){
                  
                        this.setState({orders:responseJson.result})
                            
                     }else{
  
                         showMessage(responseJson.message)
                      
                    }
                 }).catch((error) => {
                   console.log(error.message)
                           this.props.fetchCancel()
                           showMessage('Try Again')
                 });
    
  
   }
  

   
    
    onProductsClick = (item) =>{
      console.log('onProductsClick--------',JSON.stringify(item))
    
    }


  
   _renderItemProducts = ({item, index}) =>{
   
   return (
      <MyOrdersComponent
       object = {item} 
       index ={index}
       {...this.props}
       clickHandler ={this.onProductsClick}/>
   )
 }

 componentWillMount(){
   this._fetchMyOrders()
 }
    
  
      render() {
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                     <Header  label={'My Orders'} {...this.props}/>
                  
                      <KeyboardAwareScrollView
                      showsVerticalScrollIndicator={false}
                      contentContainerStyle={styles.scrollContainer}>
                     

                        <View style={{padding:10,width:dimensions.SCREEN_WIDTH}}>

                            <FlatList
                            style={{marginVertical:10}}
                            data={this.state.orders}
                            showsVerticalScrollIndicator={false}
                            renderItem={(item,index) => this._renderItemProducts(item,index)}
                            keyExtractor={item => item.id}
                            />


                            {
                              this.state.orders.length > 0
                              ?
                                
                                  <FlatList
                                  style={{marginVertical:10}}
                                  data={this.state.orders}
                                  showsVerticalScrollIndicator={false}
                                  renderItem={(item,index) => this._renderItemProducts(item,index)}
                                  keyExtractor={item => item.id}
                                  />
                            
                                  : 
                                  this.props.apiReducer.isFetching ?
                                  null
                                  :
                            
                                    <View style={{justifyContent:'center',alignItems:'center',height:dimensions.SCREEN_HEIGHT  * 0.85}}>
                                      <FastImage 
                                      source={require('../assets/no-orders.png')}
                                      style={{width:200,height:200,alignSelf:'center'}} 
                                      resizeMode={FastImage.resizeMode.contain}/>
                                  </View>
 
                            }


                        </View>
                    </KeyboardAwareScrollView>
                    { this.props.apiReducer.isFetching && <ProgressBar/> }     
          </SafeAreaView>
        </>
      );
      }
  }

  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  export default connect(mapStateToProps, mapDispatchToProps)(MyOrders);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      alignItems:'center',
      flexGrow:1,
      backgroundColor:colors.PAGE_BACKGROUND   
    },
    searchHeader:{
        overflow:'hidden',
        elevation:0
    },
    categoriesContainer:{
      padding:10,
      backgroundColor:'white',
      elevation:2,
      marginVertical:10,
      shadowColor: '#000000',
      shadowOffset: { width: 1, height: 1 },
      shadowOpacity:  0.2,
    },
    label:{marginTop:15,color:'grey',fontSize:18,fontWeight:'bold'}
   
  })
  
  