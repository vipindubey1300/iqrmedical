
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,FlatList} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  
  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import UnderlineTextInput from '../components/UnderlineTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import Header from '../components/Header';
  import ProgressBar from '../components/ProgressBar';

  
  //redux
  import { connect } from 'react-redux';
  import { addUser ,apiRequest,apiResponse} from '../actions/actions';

 class MyAddress extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        address:[]
        
      }
    }


    _deleteAddress = (addressId) =>{
      var formData = new FormData();
      formData.append('userid',this.props.rootReducer.user.id);
      formData.append('address_id',addressId);
    
      this.props.fetch()
      let url = urls.BASE_URL +'api_delete_address'
            
         fetch(url, {
             method: 'POST',
             body: formData
            }).then((response) => response.json())
                 .then( (responseJson) => {
                  this.props.fetchCancel()
                  console.log(responseJson)
                  if (!responseJson.error){

                       this.setState({
                        address: this.state.address.filter((obj) => obj.id !== addressId)
                      });
                            
                     }else{
  
                       showMessage(responseJson.message)
                      
                    }
                 }).catch((error) => {
                           console.log(error.message)
                           this.props.fetchCancel()
                           showMessage('Try Again')
                 });
    
  
   }

    _fetchAddress = () =>{
      var formData = new FormData();
      formData.append('userid',this.props.rootReducer.user.id);
    
      this.props.fetch()
      let url = urls.BASE_URL +'api_get_address'
            
         fetch(url, {
             method: 'POST',
             body: formData
            }).then((response) => response.json())
                 .then( (responseJson) => {
                  this.props.fetchCancel()
                  console.log(responseJson)
                  if (!responseJson.error){

                      this.setState({address:responseJson.result})
                            
                     }else{
  
                         showMessage('Error is fetching address.')
                      
                    }
                 }).catch((error) => {
                   console.log(error.message)
                           this.props.fetchCancel()
                           showMessage('Try Again')
                 });
    
  
   }

   componentWillMount(){
     this._fetchAddress()
   }

   componentDidMount(){
    this.props.navigation.addListener('willFocus',this._fetchAddress);
   }


   _renderItemAddress = ({item,index})=>{
     const address = item.address_title + "," + item.address_line + "," +item.address_line2 + "," +
     item.city + "," + item.state + "," + item.country + "," +item.zip_code

    return (
      <View style={styles.listContainer}>
      <Text style={{fontWeight:'bold',fontSize:16,marginVertical:3}}>{item.name}</Text>
      <Text style={{fontSize:14,marginVertical:3}}>{address}</Text>
      <Text style={{fontSize:12,marginVertical:3,color:'grey'}}>{item.mobile}</Text>

      <TouchableOpacity onPress={()=> this._deleteAddress(item.id)}
       style={{flexDirection:'row',alignItems:'center',marginVertical:5}}>
      <FastImage 
      source={require('../assets/delete.png')}
      style={{height:15,width:15}} 
      resizeMode={FastImage.resizeMode.cover}/>
      <Text  style={{fontWeight:'bold',color:'grey',fontSize:12,marginLeft:5}}>Delete</Text>

  </TouchableOpacity>

         
      </View>      
      );
}
   
      _onAdd =()=>{
          this.props.navigation.navigate("AddAddress")
      }
  
      render() {
          
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                     <Header  label={'My Address'} {...this.props}/>
                  
                      <KeyboardAwareScrollView
                      showsVerticalScrollIndicator={false}
                      contentContainerStyle={styles.scrollContainer}>

                        <View style={{padding:10,width:dimensions.SCREEN_WIDTH,alignItems:'center'}}>


                        {
                          this.state.address.length > 0
                          ?
                            
                          <FlatList
                          style={{marginBottom:70}}
                          data={this.state.address}
                          showsVerticalScrollIndicator={false}
                          renderItem={(item,index) => this._renderItemAddress(item,index)}
                          keyExtractor={item => item.id}
                            />
                        
                              : 
                              this.props.apiReducer.isFetching ?
                              null
                              :
                        
                                <View style={{justifyContent:'center',alignItems:'center',height:dimensions.SCREEN_HEIGHT  * 0.85}}>
                                  <FastImage 
                                  source={require('../assets/no-address.png')}
                                  style={{width:200,height:200,alignSelf:'center'}} 
                                  resizeMode={FastImage.resizeMode.contain}/>
                              </View>

                        }
                       
                       
                       
                       
                       

                        </View>

                        <View style={styles.absoluteContainer}>
                                <ButtonComponent 
                                style={{width :dimensions.SCREEN_WIDTH * 0.7,alignSelf:'center'}}
                                handler={this._onAdd}
                                label ={'Add Address'}/>
                          </View>
                    </KeyboardAwareScrollView>
                    { this.props.apiReducer.isFetching && <ProgressBar/> }     
          </SafeAreaView>
        </>
      );
      }
  }

  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(MyAddress);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      alignItems:'center',
      flexGrow:1,
      backgroundColor:colors.PAGE_BACKGROUND   
    },
    absoluteContainer:{
        position:'absolute',
        left:0,
        right:0,
        bottom:0,
        backgroundColor:'rgba(0,0,0,0.1)',
        padding:5
    },
    listContainer:{
      width:dimensions.SCREEN_WIDTH * 0.95,
      paddingHorizontal:7,
      paddingVertical:7,
      backgroundColor:'white',
      marginVertical:5
     
    },
   
   
  })
  
  