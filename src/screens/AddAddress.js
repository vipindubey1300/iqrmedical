
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,FlatList} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  
  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import UnderlineTextInput from '../components/UnderlineTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import Header from '../components/Header';
  import ProgressBar from '../components/ProgressBar';

  
  //redux
  import { connect } from 'react-redux';
  import { addUser ,apiRequest,apiResponse} from '../actions/actions';

 class AddAddress extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        
      }
    }


    _isValid(){
      const name = this.nameInput.getInputValue();
      const mobile = this.mobileInput.getInputValue();
      const flat = this.flatInput.getInputValue();
      const addressOne = this.addressOneInput.getInputValue()
      const addressTwo = this.addressTwoInput.getInputValue();
      const state = this.stateInput.getInputValue();
      const city = this.cityInput.getInputValue();
      const country = this.countryInput.getInputValue()
      const zip = this.zipInput.getInputValue();


   
     

      var regexp = /^\d*\.?\d*$/;
      var mobileIsNum  = regexp.test(mobile);
      var zipIsNum  = regexp.test(zip);
  
  
    if(name.trim().length == 0){
      showMessage('Enter  Name')
       return false
    }
   
    else if(mobile.trim().length == 0){
      showMessage('Enter Mobile No')
      return false
  
    }
    else if(mobile.trim().length < 10){
      showMessage('Enter Valid Mobile No')
      return false
  
    }else if(!mobileIsNum){
      showMessage('Enter Valid Mobile No')
      return false
  
    }
    if(flat.trim().length == 0){
      showMessage('Enter  Flat Number')
       return false
    }
    if(addressOne.trim().length == 0){
      showMessage('Enter Address Line One')
       return false
    }
    
    if(city.trim().length == 0){
      showMessage('Enter  City')
       return false
    }
    if(state.trim().length == 0){
      showMessage('Enter State')
       return false
    }
    if(country.trim().length == 0){
      showMessage('Enter Country')
       return false
    }
   
    else if(zip.trim().length == 0){
      showMessage('Enter Zip Code')
      return false
  
    }else if(!zipIsNum){
      showMessage('Enter Valid Zip Code')
      return false
  
    }
    
    else{
      return true;
    }
  
  
  }

    _onAdd =()=>{
  if(this._isValid()){
  
        Keyboard.dismiss()
    
        const name = this.nameInput.getInputValue();
        const mobile = this.mobileInput.getInputValue();
        const flat = this.flatInput.getInputValue();
        const addressOne = this.addressOneInput.getInputValue()
        const addressTwo = this.addressTwoInput.getInputValue();
        const state = this.stateInput.getInputValue();
        const city = this.cityInput.getInputValue();
        const country = this.countryInput.getInputValue()
        const zip = this.zipInput.getInputValue();


          this.props.fetch()
          var formData = new FormData();
        
         formData.append('userid',this.props.rootReducer.user.id);
         formData.append('name',name);
         formData.append('mobile', mobile);
         formData.append('address_title',flat);
         formData.append('address_line',addressOne);
         formData.append('address_line2', addressTwo);
         formData.append('city',city);
         formData.append('state',state);
         formData.append('country', country);
         formData.append('zip_code', zip);
    
         let url = urls.BASE_URL +'api_add_address'
               
         fetch(url, {
                 method: 'POST',
                 body: formData
                }).then((response) => response.json())
                     .then( (responseJson) => {
                      this.props.fetchCancel()
      
                      if (!responseJson.error){
      
                          showMessage(responseJson.message,false)
                          var result  = this.props.navigation.getParam('result')
                         // var path_name = result.path_name
                          if(result == undefined){
                           
                            this.props.navigation.navigate("MyAddress")
                           // this.props.navigation.pop()
                          }
                          else{
                            this.props.navigation.pop()
                            this.props.navigation.navigate(result.path_name)
                          }
                          
                                
                      }else{
    
                           showMessage(responseJson.message)
                          
                      }
                     }).catch((error) => {
                               console.log(error.message)
                               this.props.fetchCancel()
                               showMessage('Try Again')
      
                     });
        }

    }
   
 
  
      render() {
          
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                     <Header  label={'Add Address'} {...this.props}/>
                  
                      <KeyboardAwareScrollView
                      showsVerticalScrollIndicator={false}
                      contentContainerStyle={styles.scrollContainer}>

                        <View style={{padding:10,width:dimensions.SCREEN_WIDTH,alignItems:'center'}}>

                        <UnderlineTextInput
                        placeholder={'Name'}
                        onSubmitEditing={()=> this.mobile.focus()}
                        inputRef={ref => this.name = ref}
                        ref={ref => this.nameInput = ref}
                        style={{width:'90%',marginTop:10 }}
                        autoCapitalize = 'words'
                        returnKeyType="next"
                        />

                        <UnderlineTextInput
                        placeholder={'Mobile number'}
                        keyboardType="numeric"
                        textContentType='telephoneNumber'
                        maxLength={15}
                        onSubmitEditing={()=> this.flat.focus()}
                        inputRef={ref => this.mobile = ref}
                        ref={ref => this.mobileInput = ref}
                        style={{width:'90%',marginTop:10 }}
                        returnKeyType="next"
                        />



                        <UnderlineTextInput
                            placeholder={'Flat No./ Apartment'}
                            onSubmitEditing={()=> this.addressOne.focus()}
                            inputRef={ref => this.flat = ref}
                            ref={ref => this.flatInput = ref}
                            style={{width:'90%',marginTop:10 }}
                            autoCapitalize = 'words'
                            returnKeyType="next"
                        />

                        <UnderlineTextInput
                            placeholder={'Address Line One'}
                            onSubmitEditing={()=> this.addressTwo.focus()}
                            inputRef={ref => this.addressOne = ref}
                            ref={ref => this.addressOneInput = ref}
                            style={{width:'90%',marginTop:10 }}
                            autoCapitalize = 'words'
                            returnKeyType="next"
                        />

                        <UnderlineTextInput
                        placeholder={'Address Line Two'}
                        onSubmitEditing={()=> this.city.focus()}
                        inputRef={ref => this.addressTwo = ref}
                        ref={ref => this.addressTwoInput = ref}
                        style={{width:'90%',marginTop:10 }}
                        autoCapitalize = 'words'
                        returnKeyType="next"
                        />

                       

                        <UnderlineTextInput
                        placeholder={'City'}
                        onSubmitEditing={()=> this.state.focus()}
                        inputRef={ref => this.city = ref}
                        ref={ref => this.cityInput = ref}
                        style={{width:'90%',marginTop:10 }}
                        autoCapitalize = 'words'
                        returnKeyType="next"
                        />

                        <UnderlineTextInput
                        placeholder={'State'}
                        onSubmitEditing={()=> this.country.focus()}
                        inputRef={ref => this.state = ref}
                        ref={ref => this.stateInput = ref}
                        style={{width:'90%',marginTop:10 }}
                        autoCapitalize = 'words'
                        returnKeyType="next"
                        />

                        <UnderlineTextInput
                        placeholder={'Country'}
                        onSubmitEditing={()=> this.zip.focus()}
                        inputRef={ref => this.country = ref}
                        ref={ref => this.countryInput = ref}
                        style={{width:'90%',marginTop:10 }}
                        autoCapitalize = 'words'
                        returnKeyType="next"
                        />

                        <UnderlineTextInput
                        placeholder={'Zip Code'}
                        keyboardType="numeric"
                        textContentType='telephoneNumber'
                        maxLength={8}
                        onSubmitEditing={()=> this._onAdd()}
                        inputRef={ref => this.zip = ref}
                        ref={ref => this.zipInput = ref}
                        style={{width:'90%',marginTop:10 }}
                        returnKeyType="go"
                        />


                           

                       
                        <ButtonComponent 
                        style={{width :dimensions.SCREEN_WIDTH * 0.92,marginTop:40}}
                        handler={this._onAdd}
                        label ={'Add'}/>

                        </View>
                    </KeyboardAwareScrollView>
                    { this.props.apiReducer.isFetching && <ProgressBar/> }     
          </SafeAreaView>
        </>
      );
      }
  }

  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(AddAddress);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      alignItems:'center',
      flexGrow:1,
      backgroundColor:colors.PAGE_BACKGROUND   
    },
   
   
  })
  
  