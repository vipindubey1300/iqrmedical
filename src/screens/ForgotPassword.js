
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  
  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import CustomTextInput from '../components/CustomTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import LoginHeader from '../components/LoginHeader';
  import ProgressBar from '../components/ProgressBar';

  
//redux
import { connect } from 'react-redux';
import { apiRequest,apiResponse} from '../actions/actions';
    
  
class ForgotPassword extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false
      }
    }
    
  
    forgotHandler = () =>{
        this.props.navigation.navigate('Otp')
    }


    validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }
  
    _isValid(){
        const email = this.emailInput.getInputValue();
    
    
      if(email.trim().length == 0){
        showMessage('Enter Email')
        return false
    
      }
      else if(!this.validateEmail(email)){
        showMessage('Enter Valid Email')
        return false
    
      }
     
    
      else{
        return true;
      }
    
    
    }
    
   
    
      _onForgot = () =>{

        if(this._isValid()){


        Keyboard.dismiss()
        const email = this.emailInput.getInputValue();
          
         this.props.fetch()
          var formData = new FormData();
        
         formData.append('email',email);
         formData.append('device_token', 'jaBBD87dg7D');
         Platform.OS =='android'
         ?  formData.append('device_type',1)
         : formData.append('device_type',2)
      
      
           console.log("FFF",JSON.stringify(formData))
      
      
                 let url = urls.BASE_URL +'api_forgetpassword'

              fetch(url, {
                 method: 'POST',
                 body: formData
                }).then((response) => response.json())
                     .then( (responseJson) => {
                       this.props.fetchCancel()
      
                         console.log("FFF",JSON.stringify(responseJson))
  
      
                      if (!responseJson.error){
                          var id = responseJson.result.id
                          var email = responseJson.result.email
                       
                         showMessage(responseJson.message,false)

                          let tempObject = {
                             'email':email, 
                           }

                           this.props.navigation.navigate("Otp",{result:tempObject})
                                
                       }else{
    
                             showMessage(responseJson.message)
                          
                         }
                     }).catch((error) => {
                       console.log(error.message)
                       this.props.fetchCancel()
                               showMessage('Try Again')
      
                     });
        }
      
       }
  
  
      render() {
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                  {/* position absolute make the image to go in safe are so wrapping in view solve this issue */}
                  {/* image background */}
                  <View>
                    <Image source={require('../assets/signin-bg.jpg')} 
                      resizeMode="stretch"
                      style={styles.imageContainer}/>
                   </View>
                  {/* image background  end*/}

                  <LoginHeader {...this.props}/>
  
  
                      <KeyboardAwareScrollView
                      contentContainerStyle={styles.scrollContainer}>
  
                      <FastImage 
                        source={require('../assets/logo.png')}
                        style={styles.headerLogo} 
                        resizeMode={FastImage.resizeMode.contain}/>

                        <Text style={styles.headingText}>FORGOT PASSWORD</Text>

                        <View style={styles.rootContainer}>
                        <CustomTextInput
                            placeholder={'Email'}
                            onSubmitEditing={()=> this._onForgot()}
                            inputRef={ref => this.email = ref}
                            ref={ref => this.emailInput = ref}
                            style={{width:'90%'}}
                            returnKeyType="go"
                          />
                      
  
                        <ButtonComponent 
                        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:40}}
                        handler={this._onForgot}
                        label ={'Next'}/>

                      </View>
                 
                    
                    </KeyboardAwareScrollView>
  
                    { this.props.apiReducer.isFetching && <ProgressBar/> }
               
          </SafeAreaView>
        </>
      );
      }
  }

    
const mapDispatchToProps = dispatch => {
  return {
   
      fetch:() => dispatch(apiRequest({})),
      fetchCancel:() => dispatch(apiResponse({})),
  }
}
const mapStateToProps = state => {
  return {  
    apiReducer:state.apiReducer
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    imageContainer:{
        position: 'absolute',
        flex: 1,
        backgroundColor:'rgba(0,0,0,0.45)',
        width:dimensions.SCREEN_WIDTH,
        height: dimensions.SCREEN_HEIGHT
    },
  
    scrollContainer:{
      padding:10,
      alignItems:'center'
    },
    headerLogo:{
        height: 120, 
        width:dimensions.SCREEN_WIDTH * 0.6, 
        margin:10
  
    },
    headingText:{
      color:colors.BLACK,
      fontSize:18,
      marginVertical:25,
      fontWeight:'bold',
      textAlign:'center',
      color:'white'
    },
    rootContainer:{
        width:dimensions.SCREEN_WIDTH * 0.9,
        backgroundColor:'white',
        alignItems:'center',
        marginVertical:20,
        padding:10,
        borderRadius:15
      
    },
  })
  
  