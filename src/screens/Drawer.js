import React from 'react';
import { Button } from 'react-native-elements';
import { StackActions, NavigationActions} from 'react-navigation';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Image,
    TouchableOpacity,
    Alert,
    Dimensions,
    ToastAndroid
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import RBSheet from "react-native-raw-bottom-sheet";
import Snackbar from 'react-native-snackbar';
import {colors,urls,dimensions} from '../utils/constants';
import FastImage from 'react-native-fast-image'

import {showMessage} from '../utils/snackmsg';


import {removeUser } from '../actions/actions';
import { connect } from 'react-redux';



class Drawer extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
         loading_status:false,
        };

    }


  

    remove = async() =>{
        try {
         
          this.props.remove({});
      
          await AsyncStorage.removeItem("id");
          await AsyncStorage.removeItem("name");
          await AsyncStorage.removeItem("image");
          await AsyncStorage.removeItem("email");
          await AsyncStorage.removeItem("phone");
          
         this.props.navigation.navigate("Login")
         showMessage("Logged Out Successfully.",false)
          return true;
        }
        catch(exception) {
          
          return false;
        }
      }

      logout = async() =>{

        Alert.alert(
          'Logout',
          'Are you sure to logout ?',
          [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel'
          },
          {
            text: 'OK',
            onPress: () => this.remove()
          }
          ],
          {
          cancelable: false
          }
        );



      }


    render() {
        return (
                <SafeAreaView style={{flex:1,backgroundColor:colors.COLOR_PRIMARY,elevation:100}}>
                <ScrollView showsVerticalScrollIndicator={false}
                 style={{flex:1,backgroundColor:'white'}}>

                 <View style={styles.headingView}>
                 <Text onPress={()=> {
                  this.props.navigation.navigate('Settings');

                   const resetAction = StackActions.reset({
                   index: 0,
                   key: 'Settings',
                   actions: [NavigationActions.navigate({ routeName: 'Settings' })],
                 });

                 this.props.navigation.dispatch(resetAction);
                }}
                  style={{fontWeight:'bold',color:'white',fontSize:20,marginVertical:2}}>{this.props.rootReducer.user.name}</Text>
                 <Text style={{fontWeight:'500',color:'white',fontSize:15,marginVertical:2}}>{this.props.rootReducer.user.email}</Text>
                 </View>

                 <View style={{height:25}}/>


                 <TouchableOpacity style={styles.labelContainer}
                 onPress={()=> {
                 this.props.navigation.navigate('Home');

                  const resetAction = StackActions.reset({
                  index: 0,
                  key: 'Home',
                  actions: [NavigationActions.navigate({ routeName: 'Home' })],
                });

                this.props.navigation.dispatch(resetAction);
               }}>
                    <FastImage 
                    source={require('../assets/home.png')}
                    style={styles.labelImage} 
                    resizeMode={FastImage.resizeMode.cover}/>
                  <Text style={styles.labelText}>Home</Text>
              </TouchableOpacity>

                      <TouchableOpacity style={styles.labelContainer}
                      onPress={()=> {
                      this.props.navigation.navigate('MyAccount');

                      const resetAction = StackActions.reset({
                      index: 0,
                      key: 'MyAccount',
                      actions: [NavigationActions.navigate({ routeName: 'MyAccount' })],
                    });

                    this.props.navigation.dispatch(resetAction);
                    }}>
                        <FastImage 
                        source={require('../assets/my-account.png')}
                        style={styles.labelImage} 
                        resizeMode={FastImage.resizeMode.cover}/>
                      <Text style={styles.labelText}>My Account</Text>
                  </TouchableOpacity>



                  <TouchableOpacity style={styles.labelContainer}
                  onPress={()=> {
                 // this.props.navigation.navigate('Notifications');
 
                   const resetAction = StackActions.reset({
                   index: 0,
                   key: 'Notifications',
                   actions: [NavigationActions.navigate({ routeName: 'Notifications' })],
                 });
 
                 this.props.navigation.dispatch(resetAction);
                }}>
                     <FastImage 
                     source={require('../assets/notification.png')}
                     style={styles.labelImage} 
                     resizeMode={FastImage.resizeMode.cover}/>
                   <Text style={styles.labelText}>Notifications</Text>
               </TouchableOpacity>



               <TouchableOpacity style={styles.labelContainer}
               onPress={()=> {
             

                const resetAction = StackActions.reset({
                index: 0,
                key: 'MyOrders',
                actions: [NavigationActions.navigate({ routeName: 'MyOrders' })],
              });

              this.props.navigation.dispatch(resetAction);
             }}>
                  <FastImage 
                  source={require('../assets/my-orders.png')}
                  style={styles.labelImage} 
                  resizeMode={FastImage.resizeMode.cover}/>
                <Text style={styles.labelText}>My Orders</Text>
            </TouchableOpacity>


            <TouchableOpacity style={styles.labelContainer}
            onPress={()=> {
              this.props.navigation.navigate('ContactUs');
              const resetAction = StackActions.reset({
              index: 0,
              key: 'ContactUs',
              actions: [NavigationActions.navigate({ routeName: 'ContactUs' })],
            });
            this.props.navigation.dispatch(resetAction);
            }}>
               <FastImage 
               source={require('../assets/message.png')}
               style={styles.labelImage} 
               resizeMode={FastImage.resizeMode.cover}/>
             <Text style={styles.labelText}>Contact Us</Text>
         </TouchableOpacity>



         <TouchableOpacity style={styles.labelContainer}
         onPress={()=> {
         this.props.navigation.navigate('Privacy');

          const resetAction = StackActions.reset({
          index: 0,
          key: 'Privacy',
          actions: [NavigationActions.navigate({ routeName: 'Privacy' })],
        });

        this.props.navigation.dispatch(resetAction);
       }}>
            <FastImage 
            source={require('../assets/lock.png')}
            style={styles.labelImage} 
            resizeMode={FastImage.resizeMode.cover}/>
          <Text style={styles.labelText}>Privacy Policy</Text>
      </TouchableOpacity>


      <TouchableOpacity style={styles.labelContainer}
      onPress={()=> {this.logout()}}>
         <FastImage 
         source={require('../assets/logout.png')}
         style={styles.labelImage} 
         resizeMode={FastImage.resizeMode.cover}/>
       <Text style={styles.labelText}>Logout</Text>
   </TouchableOpacity>



                

                


                </ScrollView>

                </SafeAreaView>
        )
    }
}


const mapStateToProps = state => {
	return {      rootReducer: state.rootReducer,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    remove : (userinfo) => dispatch(removeUser(userinfo)),   
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Drawer);

const styles = StyleSheet.create({
        headingView:{
            height:140,
            width:'100%',
            backgroundColor:'#6E95D3',
            justifyContent:'center',
            paddingLeft:20
        },
        labelContainer:{
            height:40,
            width:'90%',
            alignItems:'center',
            paddingLeft:0,
            flexDirection:'row',
            borderBottomColor:colors.COLOR_PRIMARY,
            borderBottomWidth:2,
            alignSelf:'center',
            marginVertical:4
        },
        labelText:{
            color:'black',
            fontSize:18,
            textAlignVertical:'center',
            fontWeight:'600'

        },
        labelImage:{
            height:20,
            width:20,
            marginRight:10
        }
       

});
