
import {Text, View, Image,Dimensions,ToastAndroid,
  StatusBar,StyleSheet, TouchableOpacity,
   SafeAreaView,Alert,ImageBackground,ScrollView, Keyboard} from 'react-native';
import React from 'react';
import FastImage from 'react-native-fast-image'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AsyncStorage from '@react-native-community/async-storage';

//utils
import {showMessage} from '../utils/snackmsg';
import {colors,urls,dimensions} from '../utils/constants';

//components
import LoginTab from '../components/LoginTab';
import ProgressBar from '../components/ProgressBar';


import SignIn from './SignIn';
import SignUp from './SignUp';



//redux
import { connect } from 'react-redux';
import { addUser ,apiRequest,apiResponse} from '../actions/actions';
    



class LoginContainer extends React.Component {

    constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      selectedTab:'signin',
    }
  }
  

  loginHandler =() =>{
   this.props.navigation.navigate("Home")
  }




 componentDidMount(){
    const selectedTab = this.header.getSelectedTab()
     this.setState({selectedTab})
 }
  
 _onTabSelect = (selectedTab) =>{
     // console.log('selectedTab------',selectedTab)//home//information
      this.setState({selectedTab})
  }

  _onForgotPassword =()=>{
      this.props.navigation.navigate("ForgotPassword")
  }

  _startApiCall =()=>{
    this.props.fetch()
}

_endApiCall =()=>{
  this.props.fetchCancel()
}



    render() {
    return (
      <>
      <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
        <SafeAreaView style={styles.container}>
                {/* position absolute make the image to go in safe are so wrapping in view solve this issue */}
                {/* image background */}
                <View>
                  <Image source={require('../assets/signin-bg.jpg')} 
                    resizeMode="stretch"
                    style={styles.imageContainer}/>
                 </View>
                {/* image background  end*/}


                    <KeyboardAwareScrollView
                    contentContainerStyle={styles.scrollContainer}>

                    <FastImage 
                    source={require('../assets/logo.png')}
                    style={styles.logoImage} 
                    resizeMode={FastImage.resizeMode.contain}/>


                     {/* Header */}
                      <LoginTab 
                      handler={this._onTabSelect}
                      ref={ref => this.header = ref} {...this.props}/>
                     {/* Header  End*/}

                      {
                      this.state.selectedTab == 'signin'
                      ? 
                          <SignIn {...this.props} 
                          startApiCall = { this._startApiCall}
                          endApiCall = { this._endApiCall}
                          forgotHandler = { this._onForgotPassword}/>
                      : 
                          <SignUp {...this.props} 
                          startApiCall = { this._startApiCall}
                          endApiCall = { this._endApiCall}/>
                      }


                      {
                        this.state.selectedTab == 'signup'
                        ? 
                           <View style={{flexDirection:'row'}}>
                           <Text style={{color:'white'}}>Already have an account? </Text>
                           <Text onPress={()=> this.header._onPressSignin() }
                            style={{color:'white',textDecorationLine:'underline',fontWeight:'bold'}}> Sign In </Text>
                           </View>
                        : 
                        <View style={{flexDirection:'row'}}>
                        <Text style={{color:'white'}}>Don't have an account ?</Text>
                        <Text onPress={()=> this.header. _onPressSignup()}
                         style={{color:'white',textDecorationLine:'underline',fontWeight:'bold'}}> Sign Up </Text>
                        </View>
                        }


                  
                  </KeyboardAwareScrollView>

                  { this.props.apiReducer.isFetching && <ProgressBar/> }
             
        </SafeAreaView>
      </>
    );
    }
}


  
const mapDispatchToProps = dispatch => {
  return {
      add: (user_info) => dispatch(addUser(user_info)),
      fetch:() => dispatch(apiRequest({})),
      fetchCancel:() => dispatch(apiResponse({})),
  }
}

const mapStateToProps = state => {
  return {  
    rootReducer: state.rootReducer,
    apiReducer:state.apiReducer
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);

let styles = StyleSheet.create({
  container:{
  //  height:'100%',
    flex:1,
    backgroundColor:colors.COLOR_PRIMARY
  },
  imageContainer:{
      //  position:'absolute',
      //  top:0,left:0,right:0,bottom:0


      //height and width will not go in safe area means notch me nhi 
      //  height:'100%',
      // width:'100%'

      position: 'absolute',
      flex: 1,
      backgroundColor:'rgba(0,0,0,0.45)',
      width:dimensions.SCREEN_WIDTH,
      height: dimensions.SCREEN_HEIGHT
  },

  scrollContainer:{
    padding:10,
    alignItems:'center'
  },
  logoImage:{
      height:150,
      width:200,
      marginTop:30,
      marginBottom:20
  }

})

