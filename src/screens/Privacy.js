
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,FlatList} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  import HTML from 'react-native-render-html';

  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import UnderlineTextInput from '../components/UnderlineTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import Header from '../components/Header';
  import ProgressBar from '../components/ProgressBar';

  
  //redux
  import { connect } from 'react-redux';
  import { addUser ,apiRequest,apiResponse} from '../actions/actions';

 class Privacy extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        termsHtml:''

      }
    }

    _fetch = () =>{
    
      
      
        this.setState({loading_status:true})

        var formData = new FormData();
      
       formData.append('pageid',25);
      
    
               let url = urls.BASE_URL +'api_privacypolicy'
              
               fetch(url, {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'multipart/form-data',
               },
               body: formData
              }).then((response) => response.json())
                   .then( (responseJson) => {
                       this.setState({loading_status:false})
    
                    if (!responseJson.error){
    
                    

                     
                      var termsHtml = responseJson.result.short_description
                      this.setState({termsHtml:termsHtml})
                              
                       }else{
  
                           showMessage(responseJson.message)
                        
                         }
                   }).catch((error) => {
                     console.log(error.message)
                             this.setState({loading_status:false})
                             showMessage('Try Again')
    
                   });
      
    
     }

   

     componentWillMount(){
      this._fetch()
         
    }
 
  
      render() {
          
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                     <Header  label={'Privacy Policy'} {...this.props}/>
                  
                     <View style={{flex:1,backgroundColor:'white',padding:10}}>
                     <HTML html={this.state.termsHtml} imagesMaxWidth={Dimensions.get('window').width * 0.95} />
    
                     </View>
                     { this.state.loading_status && <ProgressBar/> } 
                     </SafeAreaView>
        </>
      );
      }
  }

  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(Privacy);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      alignItems:'center',
      flexGrow:1,
      backgroundColor:colors.PAGE_BACKGROUND   
    },
   
   
  })
  
  