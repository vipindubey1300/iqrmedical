
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  
  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import CustomTextInput from '../components/CustomTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import LoginHeader from '../components/LoginHeader';

  
  
  export default class Dummy extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false
      }
    }
    
  
      render() {
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
        <SafeAreaView style={styles.container}>
        <Header  label={'Dummy'} {...this.props}/>
     
         <KeyboardAwareScrollView
         showsVerticalScrollIndicator={false}
         contentContainerStyle={styles.scrollContainer}>

           <View style={{padding:10,width:dimensions.SCREEN_WIDTH,alignItems:'center'}}>

         
           </View>
       </KeyboardAwareScrollView>
       { this.state.loading_status && <ProgressBar/> }     
    </SafeAreaView>
        </>
      );
      }
  }
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      padding:10,
      alignItems:'center',
      flexGrow:1,
      backgroundColor:'white'
    },
   
  })
  
  