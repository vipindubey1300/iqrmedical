
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,FlatList} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  
  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import UnderlineTextInput from '../components/UnderlineTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import Header from '../components/Header';
  import ProgressBar from '../components/ProgressBar';

  
  //redux
  import { connect } from 'react-redux';
  import { addUser ,apiRequest,apiResponse} from '../actions/actions';

 class ContactUs extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        
      }
    }

   
    _isValid(){
      const subject = this.subjectInput.getInputValue();
      const message = this.messageInput.getInputValue();

  
    if(subject.trim().length == 0){
      showMessage('Enter Subject')
      return false
  
    }
    
    else if(message.trim().length == 0){
      showMessage('Enter Message')
      return false
    }
  
    else{
      return true;
    }
  
  
  }
  
  
    _onContact = () =>{
      if(this._isValid()){
  
      Keyboard.dismiss()
  
      const subject = this.subjectInput.getInputValue();
      const message = this.messageInput.getInputValue();
    
        this.props.fetch()
        var formData = new FormData();
      
      formData.append('userid',this.props.rootReducer.user.id);
       formData.append('subject',subject);
       formData.append('message', message);
  
       //console.log(JSON.stringify(formData))
      
       let url = urls.BASE_URL +'api_contactus'
             
       fetch(url, {
               method: 'POST',
               body: formData
              }).then((response) => response.json())
                   .then( (responseJson) => {
                    this.props.fetchCancel()
    
                    if (!responseJson.error){
    
                        showMessage(responseJson.message,false)
                        this.props.navigation.navigate("Home")
                              
                    }else{
  
                         showMessage(responseJson.message)
                        
                    }
                   }).catch((error) => {
                             console.log(error.message)
                             this.props.fetchCancel()
                             showMessage('Try Again')
    
                   });
      }
    
     }
      
  
      render() {
          
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                     <Header  label={'Contact Us'} {...this.props}/>
                  
                      <KeyboardAwareScrollView
                      showsVerticalScrollIndicator={false}
                      contentContainerStyle={styles.scrollContainer}>

                        <View style={{padding:10,width:dimensions.SCREEN_WIDTH,alignItems:'center'}}>


                        <UnderlineTextInput
                            placeholder={'Subject'}
                            onSubmitEditing={()=> this.message.focus()}
                            inputRef={ref => this.subject = ref}
                            ref={ref => this.subjectInput = ref}
                            style={{width:'90%',marginTop:30 }}
                            returnKeyType="next"
                            />


                            <UnderlineTextInput
                                placeholder={'Message'}
                                maxLength={400}
                                multiline={true}
                                onSubmitEditing={()=> this._onContact()}
                                inputRef={ref => this.message = ref}
                                ref={ref => this.messageInput = ref}
                                style={{width:'90%',marginTop:20}}
                                returnKeyType="go"
                           />

                       
                        <ButtonComponent 
                        style={{width :dimensions.SCREEN_WIDTH * 0.92,marginTop:40}}
                        handler={this._onContact}
                        label ={'Submit'}/>

                        </View>
                    </KeyboardAwareScrollView>
                    { this.props.apiReducer.isFetching && <ProgressBar/> }     
          </SafeAreaView>
        </>
      );
      }
  }

  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(ContactUs);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      alignItems:'center',
      flexGrow:1,
      backgroundColor:colors.PAGE_BACKGROUND   
    },
   
   
  })
  
  