
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,Keyboard,FlatList} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  import AsyncStorage from '@react-native-community/async-storage';

  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import UnderlineTextInput from '../components/UnderlineTextInput';
  import ButtonComponent from '../components/ButtonComponent';
  import Header from '../components/Header';
  import ProgressBar from '../components/ProgressBar';


  //redux
  import { connect } from 'react-redux';
  import { addUser ,apiRequest,apiResponse} from '../actions/actions';

 class EditProfile extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        
      }
    }

    _fetch = () =>{
    
        this.props.fetch()
        let url = urls.BASE_URL +'api_getprofile?userid='+ this.props.rootReducer.user.id
              
           fetch(url, {
               method: 'GET',
              }).then((response) => response.json())
                   .then( (responseJson) => {
                    this.props.fetchCancel()
                    console.log(responseJson)
                    if (!responseJson.error){
                    
                          this.setState({profile_details:responseJson.result})
                          this.emailInput.setState({text:responseJson.result.email})
                          this.phoneInput.setState({text:responseJson.result.mobile})
                          this.nameInput.setState({text:responseJson.result.name})

                              
                       }else{
    
                           showMessage('Error is fetching product details.')
                        
                      }
                   }).catch((error) => {
                     console.log(error.message)
                             this.props.fetchCancel()
                             showMessage('Try Again')
                   });
      
    
     }

   
     validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
      }
  
      _isValid(){
          const name = this.nameInput.getInputValue();
          const email = this.emailInput.getInputValue();
          const phone = this.phoneInput.getInputValue()

          var regexp = /^\d*\.?\d*$/;
          var phoneIsNum  = regexp.test(phone);
      
      
        if(name.trim().length == 0){
          showMessage('Enter  Name')
           return false
        }
       
        else if(email.trim().length == 0){
          showMessage('Enter Email')
          return false
      
        }
        else if(!this.validateEmail(email)){
          showMessage('Enter Valid Email')
          return false
      
        }
        else if(phone.trim().length == 0){
          showMessage('Enter Phone No')
          return false
      
        }
        else if(phone.trim().length < 10){
          showMessage('Enter Valid Phone No')
          return false
      
        }else if(!phoneIsNum){
          showMessage('Enter Valid Phone No')
          return false
      
        }
       
        else{
          return true;
        }
      
      
      }
      
  
  
    _onSave = () =>{
      if(this._isValid()){
  
      Keyboard.dismiss()
  
      const name = this.nameInput.getInputValue();
      const email = this.emailInput.getInputValue();
      const phone = this.phoneInput.getInputValue()
    
        this.props.fetch()
        var formData = new FormData();
      
      formData.append('userid',this.props.rootReducer.user.id);
       formData.append('name',name);
       formData.append('mobile', phone);
  
    
      
       let url = urls.BASE_URL +'api_updateprofile'
             
       fetch(url, {
               method: 'POST',
               body: formData
              }).then((response) => response.json())
                   .then( async(responseJson) => {
                    this.props.fetchCancel()
    
                    if (!responseJson.error){

                        var id = responseJson.result.id
                        var name = responseJson.result.name
                        var phone = responseJson.result.mobile
                        var image = responseJson.result.user_image

                        showMessage(responseJson.message,false)
    
                        AsyncStorage.multiSet([
                          ["id", id.toString()],
                          ["email", email],
                          ["name", name],
                          ["phone", phone],
                          ["image", image],
                         
                          ]);
  
                          console.log("Saving----",id)
    
                         await this.props.add({ 
                            id: id, 
                            phone : phone,
                            image : image,
                            email:  email ,
                            name:name,
                            
                          })
                        
                        this.props.navigation.navigate("Home")
                              
                    }else{
  
                         showMessage(responseJson.message)
                        
                    }
                   }).catch((error) => {
                             console.log(error.message)
                             this.props.fetchCancel()
                             showMessage('Try Again')
    
                   });
      }
    
     }
      
     componentWillMount(){
        this._fetch()
    }

  
      render() {
          
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
          <SafeAreaView style={styles.container}>
                     <Header  label={'Edit Profile'} {...this.props}/>
                  
                      <KeyboardAwareScrollView
                      showsVerticalScrollIndicator={false}
                      contentContainerStyle={styles.scrollContainer}>

                        <View style={{padding:10,width:dimensions.SCREEN_WIDTH,alignItems:'center'}}>


                        <UnderlineTextInput
                        placeholder={'Name'}
                        onSubmitEditing={()=> this.phone.focus()}
                        inputRef={ref => this.name = ref}
                        ref={ref => this.nameInput = ref}
                        style={{width:'90%'}}
                        autoCapitalize = 'words'
                        returnKeyType="next"
                      
                         />
        
                        <UnderlineTextInput
                        placeholder={'Email'}
                        onSubmitEditing={()=> this.phone.focus()}
                        inputRef={ref => this.email = ref}
                        ref={ref => this.emailInput = ref}
                        style={{width:'90%'}}
                        returnKeyType="next"
                        editable={false}
                         />
        
                         <UnderlineTextInput
                         placeholder={'Phone'}
                         keyboardType="numeric"
                         textContentType='telephoneNumber'
                         onSubmitEditing={()=> this._onSave()}
                         inputRef={ref => this.phone = ref}
                         ref={ref => this.phoneInput = ref}
                         style={{width:'90%'}}
                         returnKeyType="go"
                          />
        

                       
                        <ButtonComponent 
                        style={{width :dimensions.SCREEN_WIDTH * 0.92,marginTop:40}}
                        handler={this._onSave}
                        label ={'Save'}/>

                        </View>
                    </KeyboardAwareScrollView>
                    { this.props.apiReducer.isFetching && <ProgressBar/> }     
          </SafeAreaView>
        </>
      );
      }
  }

  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
  
  let styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
      alignItems:'center',
      flexGrow:1,
      backgroundColor:colors.PAGE_BACKGROUND   
    },
   
   
  })
  
  