
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity,
     SafeAreaView,Alert,ImageBackground,ScrollView,FlatList} from 'react-native';
  import React from 'react';
  import FastImage from 'react-native-fast-image'
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
  import { StackActions, NavigationActions} from 'react-navigation';

  import AsyncStorage from '@react-native-community/async-storage';


  //utils
  import {showMessage} from '../utils/snackmsg';
  import {colors,urls,dimensions} from '../utils/constants';
  
  //components
  import ButtonComponent from '../components/ButtonComponent';
  import ProgressBar from '../components/ProgressBar';
  import Header from '../components/Header';
  
  
  //redux
  import { connect } from 'react-redux';
  import { addUser ,apiRequest,apiResponse} from '../actions/actions';


  class Notifications extends React.Component {
  
      constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        notifications:[]
       
      }
    }


    _fetchNotifications = () =>{
    
      this.props.fetch()
       let url = urls.BASE_URL +'api_getmynotification?userid='+ this.props.rootReducer.user.id
      //let url = urls.BASE_URL +'api_getmynotification?userid=1'

            
         fetch(url, {
             method: 'GET',
            }).then((response) => response.json())
                 .then( (responseJson) => {
                  this.props.fetchCancel()
                  console.log(responseJson)
                  if (!responseJson.error){
                  
                        this.setState({notifications:responseJson.result})
                            
                     }else{
  
                     // showMessage(responseJson.message)
                      
                    }
                 }).catch((error) => {
                   console.log(error.message)
                           this.props.fetchCancel()
                           showMessage('Try Again')
                 });
    
  
   }
  

    _renderItemNotifications = ({item,index})=>{
        return (
          <View style={styles.listContainer}>
           <FastImage 
              source={require('../assets/noti-bell.png')}
              style={styles.listImage} 
              resizeMode={FastImage.resizeMode.contain}/>

              <View style={styles.rightContainer}>
                <View style={styles.rightTop}>
                <Text style={styles.titleText}>{item.notification_title}</Text>
                <Text style={styles.timeText}>{item.created_at.substring(11,16)}</Text>
                </View>

                <Text style={styles.messageText}>{item.notification_title}</Text>

             </View>

              <View style={styles.rightView}/>

          </View>      
          );
    }

    componentWillMount(){
      this._fetchNotifications()
    }

  
 
  render() {
      return (
        <>
        <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
        <SafeAreaView style={styles.container}>
        <Header  label={'Notifications'} {...this.props}/>

           <View style={{padding:10,width:dimensions.SCREEN_WIDTH,backgroundColor:colors.PAGE_BACKGROUND,flex:1}}>
           

             {
              this.state.notifications.length > 0
              ?
                
              <FlatList
              style={{marginVertical:10}}
              data={this.state.notifications}
              showsVerticalScrollIndicator={false}
              renderItem={(item,index) => this._renderItemNotifications(item,index)}
              keyExtractor={item => item.id}
                />
            
                  : 
                  this.props.apiReducer.isFetching ?
                  null
                  :
            
                    <View style={{justifyContent:'center',alignItems:'center',height:dimensions.SCREEN_HEIGHT  * 0.85}}>
                      <FastImage 
                      source={require('../assets/no-notifications.png')}
                      style={{width:200,height:200,alignSelf:'center'}} 
                      resizeMode={FastImage.resizeMode.contain}/>
                  </View>

            }

           </View>
      
           { this.props.apiReducer.isFetching && <ProgressBar/> }     
           </SafeAreaView>
        </>
      );
      }
  }
  const mapDispatchToProps = dispatch => {
    return {
        add: (user_info) => dispatch(addUser(user_info)),
        fetch:() => dispatch(apiRequest({})),
        fetchCancel:() => dispatch(apiResponse({})),
    }
  }
  
  const mapStateToProps = state => {
    return {  
      rootReducer: state.rootReducer,
      apiReducer:state.apiReducer
    };
  };
  export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
  
  let styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:colors.COLOR_PRIMARY
      },
      scrollContainer:{
        padding:10,
        alignItems:'center',
        flexGrow:1,
        backgroundColor:'white'
      },

      listContainer:{
        width:dimensions.SCREEN_WIDTH * 0.95,
        paddingLeft:7,
        paddingVertical:7,
        backgroundColor:'white',
        borderLeftColor:colors.COLOR_SECONDARY,
        borderLeftWidth:2,
        flexDirection:'row',
        marginVertical:5
       
      },
    listImage:{
flex:1,
  width:'100%',  
  height:50,alignSelf:'center'
  },
  rightContainer:{
    flex:8,marginHorizontal:4
  },
  rightTop:{flexDirection:'row',justifyContent:'space-between',alignItems:'center',paddingHorizontal:2},
  titleText:{fontWeight:'bold',fontSize:16},
  timeText:{color:'grey'},
  messageText:{color:'grey'},
  rightView:{
    flex:0.5,
    borderTopLeftRadius:86,
    borderBottomLeftRadius:86,
    backgroundColor:colors.COLOR_SECONDARY,
  }

     
   
  })
  
  
  
  