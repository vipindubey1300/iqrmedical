import { createStore,combineReducers } from "redux";
import rootReducer from "../reducers/rootReducer";
import apiReducer from "../reducers/apiReducer";

const mainReducer = combineReducers({
    rootReducer,
    apiReducer
  })

const store = createStore(mainReducer);


export default store;





