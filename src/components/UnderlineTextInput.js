import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform} from 'react-native';

import {colors,urls,dimensions} from '../utils/constants';

export default class UnderlineTextInput extends React.Component {
  state = {
    text: this.props.defaultValue ? this.props.defaultValue : '',
    isFocus: false,
   
  };

	componentWillReceiveProps(nextProps) {
		if (nextProps.defaultValue !== this.props.defaultValue) {
			this.setState({ text: nextProps.defaultValue.toString() });
		}
	}

  getInputValue = () => this.state.text;

  render() {
    const { isFocus, text } = this.state;
    return (
  
      <View style={[styles.container, this.props.style ,{
          borderBottomColor : isFocus ? colors.COLOR_PRIMARY : colors.DARK_GREY,
          borderBottomWidth:1.3
      }]}>
        <TextInput
          style={[styles.inputText,{height:this.props.multiline ? 100 :45}]}
          value={this.state.text}
          autoCapitalize={this.props.autoCapitalize}
          //numberOfLines={1}
          maxLength ={this.props.maxLength}
          onFocus={() => this.setState({isFocus:true})}
          onBlur={() => this.setState({isFocus:false})}
          ref={this.props.inputRef}
          secureTextEntry={this.props.secureTextEntry}
          blurOnSubmit={this.props.blurOnSubmit}
          keyboardType={this.props.keyboardType}
          returnKeyType={this.props.returnKeyType}
          placeholder={this.props.placeholder}
          textContentType={this.props.textContentType}
          onSubmitEditing={this.props.onSubmitEditing}
          placeholderTextColor={colors.DARK_GREY}
          onChangeText={this.props.onChangeText ? this.props.onChangeText : (text) => this.setState({text})}
          editable={this.props.editable}
          multiline={this.props.multiline}
          //numberOfLines={1}
          blurOnSubmit={false}
        />
        
      </View>
     
    );

   


  }
}

UnderlineTextInput.defaultProps = {
  focus: () => {},
  style: {},
  placeholder: 'Enter',
  blurOnSubmit: false,
  returnKeyType: 'next',
  keyboardType: null,
  secureTextEntry: false,
  autoCapitalize: "none",
  textContentType: "none",
  defaultValue: '',
  editable: true,
  maxLength:40,
  multiline:false
};

const styles = StyleSheet.create({
  container: {
  // backgroundColor:colors.WHITE,
   margin:0,
   overflow:'hidden',
   paddingHorizontal:2,
   paddingHorizontal:3,
   marginHorizontal:0,
   marginVertical:10

  },

  inputText: {
    textAlign:'left',
    fontSize: 16,
    color:colors.BLACK,
    marginHorizontal:3,
    height:45
  },
  imageStyle:{
      flex:1.5,
      height:45,
      width:45
    }
});