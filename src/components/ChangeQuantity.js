import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity} from 'react-native';

import {colors,urls,dimensions} from '../utils/constants';
import FastImage from 'react-native-fast-image'

export default class ChangeQuantity extends React.Component {
  state = {
    quantity : this.props.defaultValue ? this.props.defaultValue : 1
   
  };

	
  getInputValue = () => this.state.quantity;

  render() {
   
    return (
  
      <View style={[styles.container, this.props.style]}>

         <TouchableOpacity onPress={()=>{
           if(this.state.quantity > 1){
            this.setState(prevState => ({
              quantity: prevState.quantity - 1
            }));
           }
        }}
          style={{marginHorizontal:5}}>
            <FastImage 
            source={require('../assets/minus.png')}
            style={styles.imageStyle} 
            resizeMode={FastImage.resizeMode.cover}/>
          </TouchableOpacity>


            <Text style={styles.quantityText} >{this.state.quantity}</Text>

            <TouchableOpacity onPress={()=>{
              this.setState(prevState => ({
                quantity: prevState.quantity + 1
              }));
            }}
            style={{marginHorizontal:5}}>
            <FastImage 
            source={require('../assets/plus.png')}
            style={styles.imageStyle} 
            resizeMode={FastImage.resizeMode.cover}/>
          </TouchableOpacity>


      
       
        
      </View>
     
    );

   


  }
}

ChangeQuantity.defaultProps = {
 
  style: {},
 
};

const styles = StyleSheet.create({
  container: {
   backgroundColor:colors.WHITE,
   margin:0,
   overflow:'hidden',
   paddingVertical:5,
   paddingHorizontal:5,
   marginHorizontal:10,
   marginVertical:10,
   flexDirection:'row',
   justifyContent:'space-evenly',
   alignItems:'center',
   borderColor:'grey',
   borderRadius:8,
   borderWidth:1.5,
   width:140

  },

  quantityText: {
   
    fontSize: 16,
    color:colors.BLACK,
    fontWeight:'700',
    marginHorizontal:10,
    flex:1,
    textAlign:'center'
    
  },

  imageStyle:{
      height:25,
      width:25,
      
  }
  
});