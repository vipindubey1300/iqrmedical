import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import FastImage from 'react-native-fast-image'

import { withNavigation } from 'react-navigation';

import {colors,urls,dimensions} from '../utils/constants';

export default class LoginTab extends React.Component {
    constructor(props) {
        super(props);
        this. state = {
            selectedTab:'signin'
        };
    }

    // componentDidUpdate( prevProps,prevState) {
    //   // if (nextProps.selectedTab !== this.props.selectedTab) {
    //   //   this.setState({ text: nextProps.defaultValue.toString() });
    //   // }
    //   console.log('@@@@@@@@@@',prevState)
    // }

    

    getSelectedTab = () => this.state.selectedTab;
    
    _onPressSignin =()=>{
          this.setState({ selectedTab:'signin'},()=>{
                this.props.handler('signin')
          })
    }

    _onPressSignup =()=>{
      this.setState({ selectedTab:'signup'},()=>{
        this.props.handler('signup')
     })
      
    }
   
  
    render(){
        return (
            <View style={styles.container}>


            <TouchableOpacity style={{
                 paddingHorizontal:10,
                   borderBottomColor:'white',
                   borderBottomWidth:  this.state.selectedTab == 'signin' ? 2 : 0,
                  alignItems:'center',
                  justifyContent:'center',
                  height:'100%'}}
                  onPress={this._onPressSignin}>
                  <Text style={{
                    color: this.state.selectedTab == 'signin' ? 'white' :'#eee',
                    fontSize:this.state.selectedTab == 'signin' ? 19 : 16,
                    fontWeight:this.state.selectedTab == 'signin' ? 'bold' : '200',
                    
                  }}>SIGN IN</Text>
            </TouchableOpacity>


            <TouchableOpacity style={{
                paddingHorizontal:10,
                borderBottomColor:'white',
                borderBottomWidth:  this.state.selectedTab == 'signup' ? 2 : 0,
                  alignItems:'center',
                  justifyContent:'center',
                  height:'100%'}}
                  onPress={this._onPressSignup}>
                  <Text style={{
                    color: this.state.selectedTab == 'signup' ? 'white' : '#eee',
                    fontSize:this.state.selectedTab == 'signup' ? 19 : 16,
                    fontWeight:this.state.selectedTab == 'signup' ? 'bold' : '200',
                   
                  }}>SIGN UP</Text>
            </TouchableOpacity>
               
            </View>
           );
        }
};



const styles = StyleSheet.create({

  container:{
    flexDirection:'row',
    justifyContent:'space-around',
    alignItems:'center',  
    backgroundColor:'rgba(6,45,110,0.0001)',
    width:dimensions.SCREEN_WIDTH,
    height:50,
    // shadowColor: '#000000',
    // shadowOffset: {
    //   width: 0,
    //   height: 3
    // },
    // shadowRadius: 5,
    // shadowOpacity: 1.0,
    // elevation:5,

  },
  

})
