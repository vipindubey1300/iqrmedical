import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity} from 'react-native';

import {colors,urls,dimensions} from '../utils/constants';
import FastImage from 'react-native-fast-image'

export default class CategoriesComponent extends React.Component {
  state = {
   
  };

	

  render() {
      const {object} = this.props
    return (
        <View  style={styles.container}>
        <TouchableOpacity onPress={()=> this.props.clickHandler(this.props.object)}
         style={styles.imageContainer} >
            <FastImage 
            source={{uri:urls.BASE_URL_MEDIA + object.thumbnail}}
            style={styles.imageStyle} 
            resizeMode={FastImage.resizeMode.contain}/>


           
        </TouchableOpacity>

        <Text style={{textAlign:'center'}}>{object.name}</Text>
        </View>
     
    );

  }
}



const styles = StyleSheet.create({
  container: {
   backgroundColor:colors.WHITE,
   margin:10,


  },
  imageContainer:{
    margin:0,
    overflow:'hidden',
    borderRadius:10,
    borderWidth:0.6,
    borderColor:colors.GREY,
  },

 
  imageStyle:{
    height:dimensions.SCREEN_HEIGHT * 0.1,
    width:dimensions.SCREEN_WIDTH * 0.18,
    margin:5
  },
});