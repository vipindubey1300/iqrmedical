import React from 'react';
import {Text, View, Image,Dimensions,
    ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';


import {colors,urls,dimensions} from '../utils/constants';

import {
    BallIndicator,
    BarIndicator,
    DotIndicator,
    MaterialIndicator,
    PacmanIndicator,
    PulseIndicator,
    SkypeIndicator,
    UIActivityIndicator,
    WaveIndicator
  } from 'react-native-indicators';



const ProgressBar = () => {
    return(
        <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(0, 0, 0, 0.5)', justifyContent: 'center' }
          ]}>
          <UIActivityIndicator
           color={colors.COLOR_PRIMARY}
           />
          </View>
    )
  };
  export default ProgressBar;





 