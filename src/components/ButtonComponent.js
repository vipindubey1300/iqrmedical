import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { withNavigation } from 'react-navigation';
import {colors,urls,dimensions} from '../utils/constants';
import LinearGradient from 'react-native-linear-gradient';



 const ButtonComponent = (props) => {

  function _onPress()  {
    props.handler()
  }
  
  return (
    <TouchableOpacity
    onPress={()=> _onPress() }>
        <LinearGradient 
          start={{x: 0, y: 0}} end={{x: 1, y: 0}} //start end will make gradient horizontal instead fo vertical
          colors={['#192f6a', '#3b5998', '#6A91CF']} 
          style={[styles.container,props.style]}>

        <Text style={{fontWeight:'bold',color:colors.WHITE,fontSize:17}}>{props.label.toUpperCase()}</Text>
        </LinearGradient>
            
     </TouchableOpacity>
  )
};

export default ButtonComponent

const styles = StyleSheet.create({

  container:{
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:colors.COLOR_PRIMARY,
    paddingHorizontal:15,
    paddingVertical:10,
    borderRadius:30,
    marginTop:20,
    marginBottom:10,
    
  }
})
