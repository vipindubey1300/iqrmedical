import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import FastImage from 'react-native-fast-image'
import { NavigationActions } from 'react-navigation';

import { withNavigation } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';

import {colors,urls,dimensions} from '../utils/constants';

export default class Header extends React.Component {
  
    constructor(props) {
    super(props);
    this.state={
      loading_status:false
    }
  }

   _onPressBack = () => {
    //props.navigation.pop()
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render(){
    return (
        <LinearGradient 
        start={{x: 0, y: 0}} end={{x: 1, y: 0}} //start end will make gradient horizontal instead fo vertical
        colors={['#192f6a', '#3b5998', '#4c669f']}  style={styles.container}>
        {
          this.props.haveMenu ?
                 <TouchableOpacity onPress={()=> this.props.navigation.toggleDrawer()}>
                 <FastImage 
                 source={require('../assets/menu.png')}
                 style={styles.headerImage} 
                 resizeMode={FastImage.resizeMode.cover}/>
                </TouchableOpacity>
      :
               <TouchableOpacity onPress={this._onPressBack}>
                <FastImage 
                source={require('../assets/back.png')}
                style={styles.headerImage} 
                resizeMode={FastImage.resizeMode.cover}/>
               </TouchableOpacity>
     
        }
            
     
              <Text style={[styles.labelText,{color:colors.WHITE}]}>{this.props.label}</Text>
     
             <View style={{flexDirection:'row',alignItems:'center'}}>
                     
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate("Cart")}
                    style={{marginHorizontal:5}}>
                        <FastImage 
                        source={require('../assets/cart.png')}
                        style={styles.headerImage} 
                        resizeMode={FastImage.resizeMode.cover}/>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=> this.props.navigation.navigate("Notifications")}
                    style={{marginHorizontal:5}}>
                    <FastImage 
                    source={require('../assets/noti.png')}
                    style={styles.headerImage} 
                    resizeMode={FastImage.resizeMode.cover}/>
                </TouchableOpacity>

             </View>
        </LinearGradient>
       );
  }
  
  
};
Header.defaultProps = {
  haveMenu:false,
  label:'',
  haveCartIcon:true,
  haveNotificationIcon:true
};



const styles = StyleSheet.create({

  container:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    backgroundColor:'rgba(6,45,110,0.99)',
    padding:10,
    width:dimensions.SCREEN_WIDTH * 1,
    height:50,
    shadowColor: '#000000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity:  0.4,
    elevation:10
  },
  labelText:{
    fontWeight:'bold',
    fontSize:19,
    marginLeft:dimensions.SCREEN_WIDTH * 0.09
  },
  headerImage:{
      height:20,width:20
  }
})
