import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import FastImage from 'react-native-fast-image'

import { withNavigation } from 'react-navigation';

import {colors,urls,dimensions} from '../utils/constants';

 const GreyHeader = (props) => {

  function _onPress()  {
    props.navigation.goBack()
  }
  
  return (
   <View style={styles.container}>
        <TouchableOpacity onPress={_onPress}>
            <FastImage 
            source={require('../assets/back.png')}
            style={styles.headerImage} 
            resizeMode={FastImage.resizeMode.cover}/>
        </TouchableOpacity>

         

        <View/>
   </View>
  )
};

export default GreyHeader

const styles = StyleSheet.create({

  container:{
    flexDirection:'row',
    // justifyContent:'space-between',
    alignItems:'center',
    backgroundColor:'rgba(6,45,110,0.0001)',
    padding:10,
    width:dimensions.SCREEN_WIDTH,
    height:50
  },
  
  headerImage:{
      height:20,width:20
  }
})
