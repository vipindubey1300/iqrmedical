import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity} from 'react-native';

import {colors,urls,dimensions} from '../utils/constants';
import FastImage from 'react-native-fast-image'

export default class MyOrdersComponent extends React.Component {
  state = {
   
  };

  convertDate(d){
    var parts = d.split("-");
    //console.log(JSON.stringify(parts))//["2020","04","22"]

    var months = {
    "01" :'January',
    "02" :'Febuary' ,
    "03" :'March' ,
    "04" :'April' ,
    "05" :'May' ,
    "06" :'June' ,
    "07" :'July' ,
    "08" :'August' ,
    "09" :'September' ,
    "10" :'October' ,
    "11" :'November' ,
    "12" :'December' }
    var t = parts[1]
    var newDate = parts[2]+"-"+months[parts[1]]+"-"+parts[0]
    console.log(newDate);
    return newDate;
   }

  render() {
      const {object} = this.props
      const t= this.props.object.orderdate.substring(0,10)
      const date =   this.convertDate(t)
    return (
    
        <TouchableOpacity onPress={()=> this.props.clickHandler(this.props.object)}
         style={[styles.container,this.props.style]} >

            <FastImage 
            source={{uri: urls.BASE_URL_MEDIA + object.thumbnail}}
            style={styles.imageStyle} 
            resizeMode={FastImage.resizeMode.contain}/>

            <View style={styles.textContainer} >
                <Text style={{fontWeight:'bold'}}>{object.name}</Text>
                <Text style={{fontWeight:'100',color:'grey',fontSize:12}}>{object.short_description.substring(0,90)}</Text>
                <Text  style={{fontWeight:'bold',color:colors.COLOR_PRIMARY}}>Order date : {date}</Text>
            </View>


           
       
        </TouchableOpacity>
     
    );

  }
}

MyOrdersComponent.defaultProps = {
    clickHandler: {},
  };
  

const styles = StyleSheet.create({
  container: {
   backgroundColor:colors.WHITE,
   margin:2,
   overflow:'hidden',
   borderRadius:10,
   borderWidth:0.5,
   borderColor:colors.GREY,
   width:dimensions.SCREEN_WIDTH * 0.93,
   height:90,
   alignSelf:'center',
   flexDirection:'row',
   alignItems:'center',
   padding:8


  },
  textContainer:{
    flex:8,
    marginLeft:7
   
  },

 
  imageStyle:{
    height:'100%',
    width:'100%',
    borderRadius:10,
    borderWidth:0.5,
    borderColor:colors.GREY,
    flex:2
    
  },
  
});