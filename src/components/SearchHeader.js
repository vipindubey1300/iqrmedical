import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import FastImage from 'react-native-fast-image'
import { NavigationActions } from 'react-navigation';

import { withNavigation } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import {showMessage} from '../utils/snackmsg';

import {colors,urls,dimensions} from '../utils/constants';
import { ActivityIndicator } from 'react-native-paper';

export default class SearchHeader extends React.Component {
  
    constructor(props) {
    super(props);
    this.state={
      loading_status:false,
       searchtext:''
    }
  }

   _onPress()  {
    //props.navigation.pop()
    this.props.navigation.dispatch(NavigationActions.back())
  }

  _searchProducts = () =>{
    
  if(this.state.searchtext.toString().trim().length > 0){
    this.setState({loading_status:true})
    let url = urls.BASE_URL +'api_getsearchproduct?search='+this.state.searchtext
          
       fetch(url, {
           method: 'GET',
          }).then((response) => response.json())
               .then( (responseJson) => {
                this.setState({loading_status:false,searchtext:''})

                if (!responseJson.error ){
                  if (responseJson.result.length > 0 ){
                     showMessage('Total Results ' + responseJson.result.length,false)
                     this.props.showProduct(responseJson.result) 
                  }
                 }else{
                       showMessage(responseJson.message)    
                  }
               }).catch((error) => {
                 console.log(error.message)
                 this.setState({loading_status:false})
                  showMessage(error.message)

               });
  
  }

   }


  render(){
    return (
        <LinearGradient 
            start={{x: 0, y: 0}} end={{x: 1, y: 0}} //start end will make gradient horizontal instead fo vertical
            colors={['#192f6a', '#3b5998', '#4c669f']} 
           style={[styles.container,this.props.style]}>

         <View style={styles.searchContainer}>
                <TouchableOpacity onPress = {()=> this._searchProducts()}
                 style={{marginLeft:5,flex:1}}>
                    <FastImage 
                    source={require('../assets/search.png')}
                    style={styles.searchImage} 
                    resizeMode={FastImage.resizeMode.cover}/>
                </TouchableOpacity>


                <TextInput
                    style={styles.searchInput}
                    value={this.state.otp1}
                    onChangeText={ (searchtext) => {
                        this.setState({searchtext:searchtext})
                    }}
                    onSubmitEditing={()=> this._searchProducts()}
                    maxLength={45}
                    placeholder={'Search ....'}
                    autoCorrect={false}
                    autoCapitalize={'none'}
                    placeholderTextColor={'grey'}
                 />
                 {
                   this.state.loading_status  
                   ? <ActivityIndicator 
                      animating={true} 
                      color={colors.COLOR_PRIMARY}
                      size={20}
                      style={{flex:1}}/>
                   : null
                 }
         </View>
       
        </LinearGradient>
       );
  }
  
  
};
SearchHeader.defaultProps = {
  showProduct : () => {}
};



const styles = StyleSheet.create({

  container:{
    //alignItems:'center',
    padding:10,
    width:dimensions.SCREEN_WIDTH * 1,
    height:70,
    shadowColor: '#000000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity:  0.4,
    elevation:10
   
  },
 searchContainer:{
    width:'100%',
    height:45,
    backgroundColor:'white',
    borderRadius:8,
    flexDirection:'row',
    alignItems:'center',
    overflow:'hidden'
},
searchImage:{
    height:22,
    width:22
},
searchInput: {
    flex:9,
    width:'100%',
    height: '100%',
    backgroundColor: colors.WHITE,
    color:colors.BLACK
  },
})
