import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity} from 'react-native';

import {colors,urls,dimensions} from '../utils/constants';
import FastImage from 'react-native-fast-image'

export default class CartComponent extends React.Component {
  state = {
   
  };

	

  render() {
      const {object} = this.props
    return (
    
        <TouchableOpacity onPress={()=> this.props.clickHandler(this.props.object)}
         style={[styles.container,this.props.style]} >

            <FastImage 
            source={{uri:urls.BASE_URL_MEDIA +object.product.thumbnail}}
            style={styles.imageStyle} 
            resizeMode={FastImage.resizeMode.contain}/>

            <View style={styles.textContainer} >
                <Text style={{fontWeight:'bold'}}>{object.product.name}</Text>
                <Text style={{fontWeight:'100',color:'grey',fontSize:12}}>{object.product.description.substring(0,90)}</Text>

                <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                        <Text  style={{fontWeight:'bold'}}>${object.product.price}</Text>
                        <TouchableOpacity onPress={()=> this.props.deleteHandler(this.props.object.id)}
                        style={{flexDirection:'row',alignItems:'center'}}>
                            <FastImage 
                            source={require('../assets/delete.png')}
                            style={{height:15,width:15}} 
                            resizeMode={FastImage.resizeMode.cover}/>
                            <Text  style={{fontWeight:'bold',color:'grey',fontSize:12}}>Delete</Text>

                        </TouchableOpacity>
                </View>
            </View>


           
       
        </TouchableOpacity>
     
    );

  }
}


CartComponent.defaultProps = {
    clickHandler: {},
  };


const styles = StyleSheet.create({
  container: {
   backgroundColor:colors.WHITE,
   margin:2,
   overflow:'hidden',
   borderRadius:10,
   borderWidth:0.5,
   borderColor:colors.GREY,
   width:dimensions.SCREEN_WIDTH * 0.93,
   height:90,
   alignSelf:'center',
   flexDirection:'row',
   alignItems:'center',
   padding:8


  },
  textContainer:{
    flex:8,
    marginLeft:7
   
  },

 
  imageStyle:{
    height:'100%',
    width:'100%',
    borderRadius:10,
    borderWidth:0.5,
    borderColor:colors.GREY,
    flex:2
    
  },
  
});