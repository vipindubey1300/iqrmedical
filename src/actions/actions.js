
export function addUser(payload) {
    return { type: "ADD_USER", payload }
  };
  
  export function removeUser(payload) {
    return { type: "REMOVE_USER", payload }
  };
  
  export function changeUser(payload) {
    return { type: "UPDATE_USER", payload }
  };


  export function apiRequest(payload) {
    return { type: "API_REQUEST", payload }
  };

  export function apiResponse(payload) {
    return { type: "API_RESPONSE", payload }
  };

  
  
  