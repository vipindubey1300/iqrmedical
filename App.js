
import React from 'react';

import store from './src/store/store';
import { Provider } from 'react-redux';
import RootNavigator from './RootNavigator';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView,Alert} from 'react-native';



export default class App extends React.Component {

	constructor(props) {
		super(props);
	}


	render() {
		return (
			<Provider store={store}>
					<RootNavigator/>
			</Provider>
		);
	}
}




